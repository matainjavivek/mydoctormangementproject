@extends('partials.user.header')


@section('front_title')
    {{ __('Informasi Anda') }}
@endsection
@section('content')
    <div class="daftar">

        <div class="background">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8 mx-auto">
                        <div class="daftar-contact-wrap">
                            <div class="daftar-contact">
                                <div class="daftar-contact-heading">Informasi Anda</div>
                                <form method="POST" action="{{ route('front.store') }}" onsubmit="return validationUserDetails()" name="myForm">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label label-heading">Full
                                            name</label>
                                        <input type="text" name="name" class="form-control daftar-fild-style"
                                            placeholder="Enter full name" id="exampleInputEmail1"
                                            aria-describedby="emailHelp">
                                        <div id="emailHelp" class="form-text error-text" style="display: none;">Please
                                            enter the full name</div>
                                            
                                    <span class="gu-hide" style="color: red;" id="errorname">{{__('validation.Name_required')}}</span>      
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label label-heading mt-3">Referral
                                            Code ( jika ada)</label>
                                        <input type="text" name="referral_code" class="form-control daftar-fild-style "
                                            placeholder="Enter referral code" id="referral_code"
                                            aria-describedby="emailHelp">
                                        <div id="referral_code_error_text" class="form-text error-text gu-hide">Please enter the correct
                                            referral code</div>
                                             <div class="referrer-name">Referrer name -   <span id="user-name"></span> </div>
                                    </div>
                                    <!-- <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label label-heading mt-3">Referral
                                            Code ( jika ada)</label>
                                            <input type="text" class="form-control" name="referral_code" id="referral_code" value="{{isset($referral_code)?$referral_code:''}}" placeholder="Enter referral code">
                                            <div class="referrer-name">Referrer name :-   <span id="user-name"></span> </div>
                                        <div id="errorstyled-checkbox-1"  class="form-text error-text gu-hide">Please enter the correct
                                            referral code</div>
                                    </div> -->
                                    <!-- <div class="mt-5 text-center"> -->
                                        <!-- <button type="submit">
                                                    <div class="continue_btn">Daftar</div>
                                                </button> -->
                                                <div class="text-center">
                                                    <button class="continue_btn submit_btn" type="submit">Daftar</button>
                                                </div>

                                    <!-- </div> -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('assets/js/user.js')}}"></script>
</body>

</html>

@endsection

