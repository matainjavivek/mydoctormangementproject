@extends('partials.user.header')


@section('front_title')
    {{ __('Profile') }}
@endsection
@section('content')

    <div class="daftar">


        <div class="background">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8 mx-auto">
                        <div class="daftar-contact-wrap">
                            <div class="daftar-contact">
                                <div class="profil-heading">PROFIL ANDA</div>
                                <div class="profil-sub-heading">Nama Lengkap</div>
                                <div class="profil-text">{{isset($customer->name)?$customer->name:''}}<span class="edit"><a href="{{route('front.edit',[$customer['id']])}}">EDIT</a></span></div>
                                <div class="profil-sub-heading">No Handphone</div>
                                <div class="profil-text">{{isset($customer->phone)?$customer->phone:''}}</div>
                                <div class="mt-5 text-center">

                                <a href="{{ route('authuser.customer') }}">
                                    <div class="continue_btn">Continue</div>
                                </a><br>
                                <a href="{{ route('front.logout') }}">
                                    <div class="continue_btn">Logout</div>
                                </a>
                            </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    </div>

</body>

</html>
@endsection
