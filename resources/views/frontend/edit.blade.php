@extends('partials.user.header')


@section('front_title')
    {{ __('Profile') }}
@endsection
@section('content')

    <div class="daftar">
    <form class="pl-3 pr-3" method="post" action="{{ route('front.update') }}" onsubmit="return validationEditUser()" name="myForm">
        @csrf
    <input type="hidden" class="form-control" id="id" value="{{$customer->id}}" name="id" />
        <div class="background">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8 mx-auto">
                        <div class="daftar-contact-wrap">
                            <div class="daftar-contact">
                                <div class="profil-heading">sunting profilmu</div>
                                <div class="profil-sub-heading">Nama Lengkap</div>
                                <!-- <label class="profil-text" for="name">{{__('messages.Name')}}</label> -->
                                <div >
                             <input type="text" class="profil-text" id="name" value="{{$customer->name}}" name="name" size="35"/>
                             
                            </div> 
                            <div>   
                            <span class="gu-hide" style="color: red;" id="errorname">{{__('validation.Name_required')}}</span> 
</div>
                              <button type="submit" style="background-color: cornflowerblue;
                                border-radius: inherit;">Submit</button> 
                           
                              
                        
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</form>
    </div>
    <script src="{{asset('assets/js/user.js')}}"></script>


</html>
@endsection
