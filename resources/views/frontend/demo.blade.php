<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Laravel 8 Firebase Phone Number Auth with Example</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .container {
            max-width: 500px;
            margin-top: 50px;
        }

    </style>
</head>

<body>
    <div class="container">
        <div class="alert alert-danger" id="showError" style="display: none;"></div>

        <h4>Phone number</h4>
        <div class="alert alert-success" id="authSuccess" style="display: none;"></div>

        <form>
            <div class="form-group mb-3">
                <input type="text" id="phoneNumber" class="form-control" placeholder="+91 #########">
            </div>

            <div class="form-group">
                <div id="recaptcha-container"></div>
            </div>

            <div class="d-grid mt-3">
                <button type="button" class="btn btn-dark" onclick="sendFirebasePhoneOTP();">Send OTP</button>
            </div>
        </form>


        <div class="mb-5 mt-5">
            <h3>Enter OTP</h3>
            <div class="alert alert-success" id="otpSuccess" style="display: none;"></div>
            <form>

                <input type="text" id="verfifyNumber" class="form-control" placeholder="Verification code">

                <div class="d-grid mt-3">
                    <button type="button" class="btn btn-danger mt-3" onclick="verifyPhoneNumber()">Authenticate</button>
                </div>
            </form>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>

    <script>
        // const firebaseConfig = {
        //     apiKey: "AIzaSyAx1YTyDyMOdiGo4fQWOeGkwqYhouAcb2A",
        //     authDomain: "sleep-f8d1b.firebaseapp.com",
        //     projectId: "sleep-f8d1b",
        //     storageBucket: "sleep-f8d1b.appspot.com",
        //     messagingSenderId: "435816409131",
        //     appId: "1:435816409131:web:1d59fa41695419d8b7226e",
        //     measurementId: "G-6BB59C94RQ"
        // };
        const firebaseConfig = {
            apiKey: "AIzaSyBkiaGGQWA4g8XZVMEUR1csefy1jRwoA5o",
            authDomain: "sleep-9949e.firebaseapp.com",
            projectId: "sleep-9949e",
            storageBucket: "sleep-9949e.appspot.com",
            messagingSenderId: "233754733458",
            appId: "1:233754733458:web:ee787275dd534b18762f4b",
            measurementId: "G-J4TWC43LH0"
        };

        firebase.initializeApp(firebaseConfig);
    </script>

    <script type="text/javascript">
        window.onload = function () {
            render();
        };

        function render() {
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
            recaptchaVerifier.render();
        }

        function sendFirebasePhoneOTP() {
            var phoneNumber = $("#phoneNumber").val();
            firebase.auth().signInWithPhoneNumber(phoneNumber, window.recaptchaVerifier)
            .then(function (confirmationResult) {
                window.confirmationResult = confirmationResult;
                coderesult = confirmationResult;
                $("#authSuccess").text("Message sent");
                $("#authSuccess").show();
            }).catch(function (error) {
                $("#showError").text(error.message);
                $("#showError").show();
            });

        }

        function verifyPhoneNumber() {
            var code = $("#verfifyNumber").val();
            coderesult.confirm(code).then(function (res) {
                var user = res.user;
                alert(user);
                $("#otpSuccess").text("Auth successful");
                $("#otpSuccess").show();
            }).catch(function (error) {
                $("#showError").text(error.message);
                $("#showError").show();
            });
        }
    </script>
</body>

</html>
