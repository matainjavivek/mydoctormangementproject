@extends('partials.user.header')


@section('front_title')
    {{ __('Profile') }}
@endsection
@section('content')

    <div class="doctor_search">
        {{-- <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col"><a href="{{ route('authuser.customer') }}" class="margin-left-15"><img src="{{ asset('frontend/images/logo.png')}}" class="logoW"
                                alt="logo"></a></div>
                                <div class="col text-end"><a href="{{ route('authuser.profile') }}"> <img src="{{ asset('frontend/images/user-icon.png')}}" alt="user" class="userW"></a></div>
                </div>
            </div>
            <div class="header-back"><i class="fa fa-angle-left "></i></div>
        </div> --}}

        <div class="doctor_search_block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-10 col-sm-12 mx-auto">
                        <div class="doctor_search_heading">
                            Cari Dokter Di Kota Anda
                        </div>
                        <div class="doctor_search_wrap">
                        <form action="{{ route('front.closestCarriers') }}" method="GET">
                            <input type="text" id="address" name="address" class="form-control border-0 rounded-0 doctor_search_style"
                                placeholder="City name" value="{{isset($city)?$city:''}}">
                                <input type="hidden" id="lat" name="latitude" value="{{isset($latitude)?$latitude:''}}">
                                <input type="hidden" id="lon" name="longitude" value="{{isset($longitude)?$longitude:''}}">
                                <input type="hidden" id="city" name="city" value="{{isset($city)?$city:''}}">
                            <div class="location-icon"><i class="fa-solid fa-location-dot"></i></div>

                            <div id="map" style="width: 100%; height: 0px;top:4px"></div>
                            <div class="search-icon-wrap">
                                <button type="submit" class="search-icon-button">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mainContentBg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-10 col-sm-12 mx-auto">
                        @if (isset($doctors))
                        @foreach ($doctors as $key=>$doctor)
                        <div class="dr-list-block">
                            <div class="dr-name">{{isset($doctor[0]['name'])?$doctor[0]['name']:''}}</div>
                            @if (isset($doctor))
                            @foreach ($doctor as $key=>$data)
                            <div class="adress-set">
                                <div class="store-name">{{isset($data['title'])?$data['title']:""}}</div>
                                <div class="store-address">
                                    {{isset($data['address'])?$data['address']:""}}
                                </div>
                                <div class="phIcon-block"><a href="#"><img src="{{ asset('frontend/images/ph_icon.svg')}}" alt="ph icon" width="70%"></a></div>
                            </div>
                            <div class="saparator">&nbsp;</div>
                            @endforeach
                        @endif
                            {{-- <div class="adress-set">
                                <div class="store-name">Sankari Madical Hall</div>
                                <div class="store-address">
                                    Baguihati, Aswininagar, Amarabati, Kol-102, Akshya Nagar 1st Block 1st Cross,
                                    Rammurthy nagar amarabati.
                                </div>
                                <div class="phIcon-block"><a href="#"><img src="{{ asset('frontend/images/ph_icon.svg')}}" alt="ph icon" width="70%"></a></div>
                            </div> --}}
                        </div>
                        @endforeach
                        @endif

                        {{-- <div class="dr-list-block">
                            <div class="dr-name">Dr. Soumen Kumar Saha</div>
                            <div class="adress-set">
                                <div class="store-name">Samrat Madico Madical Hall</div>
                                <div class="store-address">
                                    Baguihati, Aswininagar, Amarabati, Kol-102, Akshya Nagar 1st Block 1st Cross,
                                    Rammurthy nagar amarabati.
                                </div>
                                <div class="phIcon-block"><a href="#"><img src="{{ asset('frontend/images/ph_icon.svg')}}" alt="ph icon" width="70%"></a></div>
                            </div>
                            <div class="saparator">&nbsp;</div>
                            <div class="adress-set">
                                <div class="store-name">Sankari Madical Hall</div>
                                <div class="store-address">
                                    Baguihati, Aswininagar, Amarabati, Kol-102, Akshya Nagar 1st Block 1st Cross,
                                    Rammurthy nagar amarabati.
                                </div>
                                <div class="phIcon-block"><a href="#"><img src="{{ asset('frontend/images/ph_icon.svg')}}" alt="ph icon" width="70%"></a></div>
                            </div>
                            <div class="saparator">&nbsp;</div>
                            <div class="adress-set">
                                <div class="store-name">Rumah Sakti Mayapada Madico</div>
                                <div class="store-address">
                                    Baguihati, Aswininagar, Amarabati, Kol-102, Akshya Nagar 1st Block 1st Cross,
                                    Rammurthy nagar amarabati.
                                </div>
                                <div class="phIcon-block"><a href="#"><img src="{{ asset('frontend/images/ph_icon.svg')}}" alt="ph icon" width="70%"></a></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mob-saparator"></div>
                        </div>
                        <div class="dr-list-block">
                            <div class="dr-name">Dr. Deb Kumar Roy</div>
                            <div class="adress-set">
                                <div class="store-name">Rumah Sakti Mayapada Madico</div>
                                <div class="store-address">
                                    Baguihati, Aswininagar, Amarabati, Kol-102, Akshya Nagar 1st Block 1st Cross,
                                    Rammurthy nagar amarabati.
                                </div>
                                <div class="phIcon-block"><a href="#"><img src="{{ asset('frontend/images/ph_icon.svg')}}" alt="ph icon" width="70%"></a></div>
                            </div>
                            <div class="saparator">&nbsp;</div>
                            <div class="adress-set">
                                <div class="store-name">Sankari Madical Hall</div>
                                <div class="store-address">
                                    Baguihati, Aswininagar, Amarabati, Kol-102, Akshya Nagar 1st Block 1st Cross,
                                    Rammurthy nagar amarabati.
                                </div>
                                <div class="phIcon-block"><a href="#"><img src="{{ asset('frontend/images/ph_icon.svg')}}" alt="ph icon" width="70%"></a></div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>


    </div>


    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAwQ3Gm7MIQ61LSG4u2s6XBuZyLZMj47CE&callback=initMap" async defer></script>

    <script src="{{ asset('frontend/js/gmap.js') }}"></script>
</body>

</html>
@endsection
