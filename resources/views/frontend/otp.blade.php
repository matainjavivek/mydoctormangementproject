@include('partials.user.header')
</head>

<body>


    <div class="daftar">
        <div class="top-header display-none">
            <div class="container">
                <div class="row">
                    <div class="col"><a href="#"><img src="{{ asset('frontend/images/logo.png')}}" class="logoW"
                                alt="logo"></a></div>
                    <div class="col text-end"><img src="{{ asset('frontend/images/user-icon.png')}}" alt="user" class="userW"></div>
                </div>
            </div>
        </div>
        <div class="background">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-9 col-sm-12 mx-auto">
                        <div class="daftar-contact-wrap">
                            <div class="daftar-contact">
                                <div class="">
                                    <div class="mb-4">
                                        <div class="row">
                                            <div class="col">
                                                <div class="text-start display-block">
                                                    <i class="fa fa-angle-left "></i>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="text-end">
                                                    <a class="help_btn" href="#"><i
                                                            class="fa-regular fa-circle-question"></i> Help</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="my-auto">
                                            <div class="login_part p-3 p0">
                                                <div class="fs_18">Enter the 6-digit OTP sent to</div>
                                                <div class="pb-4 fs_18">+91 32658 96584</div>
                                                <div class="otp-box">
                                                    <form method="get" class="digit-group" data-group-name="digits"
                                                        data-autosubmit="false" autocomplete="on">
                                                        <input type="text" id="digit-1" name="digit-1"
                                                            data-next="digit-2" maxlength="1">
                                                        <input type="text" id="digit-2" name="digit-2"
                                                            data-next="digit-3" data-previous="digit-1" maxlength="1">
                                                        <input type="text" id="digit-3" name="digit-3"
                                                            data-next="digit-4" data-previous="digit-2" maxlength="1">
                                                        <input type="text" id="digit-4" name="digit-4"
                                                            data-next="digit-5" data-previous="digit-3" maxlength="1">
                                                        <input type="text" id="digit-5" name="digit-5"
                                                            data-next="digit-6" data-previous="digit-4" maxlength="1">
                                                        <input type="text" id="digit-6" name="digit-6"
                                                            data-previous="digit-5" maxlength="1">
                                                    </form>
                                                </div>
                                                <div class="code_recv pt-3">
                                                    <div class="">Didn’t receive the code?</div>
                                                    <a href="#" class="resend_btn ms-3">Resend</a>
                                                </div>
                                                <div class="pt-2 pb-5"><a class="trouble_btn" href="#">Get OTP on
                                                        call</a>
                                                    </div>
                                                <a href="{{ route('front.userdetils') }}">
                                                    <div class="continue_btn">Continue</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer display-none">
            <div class="footer-wrap">
                <div class="my-auto tentang"><a href="#">TENTANG KAMI</a></div>
                <a href="#">
                    <div class="whatsapp_btn py-2 px-3">
                        <i class="fa-brands fa-whatsapp"></i>
                        <span class="ps-2">Customer Service</span>
                    </div>
                </a>
            </div>
        </div>
    </div>






    <script>
        $('.digit-group').find('input').each(function () {
            $(this).attr('maxlength', 1);
            $(this).on('keyup', function (e) {
                var parent = $($(this).parent());

                if (e.keyCode === 8 || e.keyCode === 37) {
                    var prev = parent.find('input#' + $(this).data('previous'));

                    if (prev.length) {
                        $(prev).select();
                    }
                } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                    var next = parent.find('input#' + $(this).data('next'));

                    if (next.length) {
                        $(next).select();
                    } else {
                        if (parent.data('autosubmit')) {
                            parent.submit();
                        }
                    }
                }
            });
        });
    </script>


</body>

</html>
