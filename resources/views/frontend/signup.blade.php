@include('partials.user.header')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

    <div class="daftar">
        <div class="top-header display-none">
            <div class="container">
                <div class="row">
                    <div class="col"><a href="#"><img src="{{ asset('frontend/images/logo.png')}}" class="logoW"
                                alt="logo"></a></div>
                    <div class="col text-end"><img src="{{ asset('frontend/images/user-icon.png')}}" alt="user" class="userW"></div>
                </div>
            </div>
        </div>
        <div class="background">
            <div class="container">
                <div class="row " id="otp_send">
                    <div class="col-lg-5 col-md-9 col-sm-12 mx-auto">
                        <div class="daftar-contact-wrap">
                            <div class="daftar-contact">
                                <div class="">
                                    <div class="row">
                                        <div class="col">
                                            <div class="text-start display-block">
                                                <i class="fa fa-angle-left "></i>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="text-end">
                                                <a class="help_btn" href="#"><i
                                                        class="fa-regular fa-circle-question"></i> Help</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="my-auto">
                                            <div class="login_part p-3 p0 pt">
                                                <div class="pb-4 fs_18">Reenter your mobile number</div>

                                                <form class="">
                                                    <div class="input-group fildStyle">
                                                      <span class="input-text">
                                                        <select class="form-select border-0 rounded-0" name="country_code" id="country_code">
                                                            <option value="+62">+62</option>
                                                            <option value="+91">+91</option>
                                                            <option value="+15">+15</option>
                                                            <option value="+10">+10</option>
                                                          </select>
                                                      </span>


                                                        <input type="tel" maxlength="10" minlength="10"  class="form-control border-0 rounded-0" name="phone" id="phone" placeholder="Mobile Number">
                                                        <div>
                                                        <span class="gu-hide" style="color: red;" id="errorphone">Phone number not match as you entered previous</span>
                                                      </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div id="recaptcha-container"></div>
                                                    </div>
                                                <p class="pt-3">By continuing, you agree to our</p>
                                                <div class="pb-4"><a class="trouble_btn" href="#">Terms &amp; Conditions</a></div>
                                                <a href="#"><div type="submit" class="continue_btn" onclick="sendFirebasePhoneOTP();">Continue</div></a>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- otp submit --}}
                <div class="row gu-hide" id="otp_receive">
                    <div class="col-lg-6 col-md-9 col-sm-12 mx-auto">
                        <div class="daftar-contact-wrap">
                            <div class="daftar-contact">
                                <div class="">
                                    <div class="mb-4">
                                        <div class="row">
                                            <div class="col">
                                                <div class="text-start display-block">
                                                    <i class="fa fa-angle-left "></i>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="text-end">
                                                    <a class="help_btn" href="#"><i
                                                            class="fa-regular fa-circle-question"></i> Help</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="my-auto">
                                            <div class="login_part p-3 p0">
                                                <div class="fs_18">Enter the 6-digit OTP sent to</div>
                                                <div class="pb-4 fs_18" id="phone_number"></div>
                                                <div class="otp-box">
                                                    <div class="alert alert-success" id="otpSuccess" style="display: none;"></div>
                                                    {{-- <form method="get" class="digit-group" data-group-name="digits"
                                                        data-autosubmit="false" autocomplete="on"> --}}
                                                        <input type="text" id="digit-1" name="digit-1"
                                                            data-next="digit-2" maxlength="1">
                                                        <input type="text" id="digit-2" name="digit-2"
                                                            data-next="digit-3" data-previous="digit-1" maxlength="1">
                                                        <input type="text" id="digit-3" name="digit-3"
                                                            data-next="digit-4" data-previous="digit-2" maxlength="1">
                                                        <input type="text" id="digit-4" name="digit-4"
                                                            data-next="digit-5" data-previous="digit-3" maxlength="1">
                                                        <input type="text" id="digit-5" name="digit-5"
                                                            data-next="digit-6" data-previous="digit-4" maxlength="1">
                                                        <input type="text" id="digit-6" name="digit-6"
                                                            data-previous="digit-5" maxlength="1">
                                                    {{-- </form> --}}
                                                </div>
                                                <div class="code_recv pt-3">
                                                    <div class="">Didn’t receive the code?</div>
                                                    <a class="resend_btn ms-3" onclick="resendFirebasePhoneOTP();">Resend</a>
                                                </div>
                                                <div class="pt-2 pb-5"><a class="trouble_btn" href="#">Get OTP on
                                                        call</a>
                                                    </div>
                                                <a href="#" onclick="verifyPhoneNumber()">
                                                    <div class="continue_btn">Continue</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- end otp submit --}}
            </div>
        </div>

        <div class="footer mob_display-none">
            <div class="footer-wrap">
                <div class="my-auto tentang"><a href="#">TENTANG KAMI</a></div>
                <a href="#">
                    <div class="whatsapp_btn py-2 px-3">
                        <i class="fa-brands fa-whatsapp"></i>
                        <span class="ps-2">Customer Service</span>
                    </div>
                </a>
            </div>
        </div>
    </div>


    <script>
        $('.digit-group').find('input').each(function () {
            $(this).attr('maxlength', 1);
            $(this).on('keyup', function (e) {
                var parent = $($(this).parent());

                if (e.keyCode === 8 || e.keyCode === 37) {
                    var prev = parent.find('input#' + $(this).data('previous'));

                    if (prev.length) {
                        $(prev).select();
                    }
                } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                    var next = parent.find('input#' + $(this).data('next'));

                    if (next.length) {
                        $(next).select();
                    } else {
                        if (parent.data('autosubmit')) {
                            parent.submit();
                        }
                    }
                }
            });
        });
    </script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>

<script>

    const firebaseConfig = {
        apiKey: "AIzaSyBkiaGGQWA4g8XZVMEUR1csefy1jRwoA5o",
        authDomain: "sleep-9949e.firebaseapp.com",
        projectId: "sleep-9949e",
        storageBucket: "sleep-9949e.appspot.com",
        messagingSenderId: "233754733458",
        appId: "1:233754733458:web:ee787275dd534b18762f4b",
        measurementId: "G-J4TWC43LH0"
    };

    firebase.initializeApp(firebaseConfig);
</script>

<script type="text/javascript">
 var captcha=null;
 var captcha1=null;
    window.onload = function () {
        render();
    };

    function render() {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        recaptchaVerifier.render();
    }

    function sendFirebasePhoneOTP() {
        var countryCode = $("#country_code").val();
        var phoneNumber =countryCode+$("#phone").val();
        console.log($.type(phoneNumber));
        console.log(phoneNumber);
        console.log(oldPhoneNumber);
        $("#otp_send").addClass("gu-hide");
        $("#otp_receive").removeClass("gu-hide");
        $("#phone_number").text(phoneNumber);

        captcha =window.recaptchaVerifier;
        captcha1 = firebase.auth();
        // sessionStorage.setItem("captcha", captcha);
        // console.log(captcha1 );
        captcha1.signInWithPhoneNumber(phoneNumber, captcha)
        .then(function (confirmationResult) {
            window.confirmationResult = confirmationResult;
            coderesult = confirmationResult;
            $("#authSuccess").text("Message sent");
            $("#authSuccess").show();
        }).catch(function (error) {
            $("#showError").text(error.message);
            $("#showError").show();
        });

    }
    function resendFirebasePhoneOTP() {
        // var captcha = sessionStorage.getItem("captcha");
        var countryCode = $("#country_code").val();
        var phoneNumber =countryCode+$("#phone").val();
console.log(firebase.auth());
captcha1.signInWithPhoneNumber(phoneNumber, captcha)
        .then(function (confirmationResult) {
            window.confirmationResult = confirmationResult;
            coderesult = confirmationResult;
            console.log(phoneNumber);
            $("#authSuccess").text("Message sent");
            $("#authSuccess").show();
        }).catch(function (error) {
            console.log(phoneNumber);
            $("#showError").text(error.message);
            $("#showError").show();
        });
        console.log('captcha');
        console.log(captcha);
    }
    function verifyPhoneNumber() {
        var code2 = $("#digit-2").val();
        var code3 = $("#digit-3").val();
        var code4 = $("#digit-4").val();
        var code1 = $("#digit-1").val();
        var code5 = $("#digit-5").val();
        var code6 = $("#digit-6").val();
        var code =code1+code2+code3+code4+code5+code6;
        coderesult.confirm(code).then(function (res) {
            var route = '{{ route("front.userdetils") }}';
            window.location.href =route;
            $("#otpSuccess").text("Auth successful");
            $("#otpSuccess").show();
        }).catch(function (error) {
            $("#showError").text(error.message);
            $("#showError").show();
        });
    }
</script>
<script>
    $('.digit-group').find('input').each(function () {
        $(this).attr('maxlength', 1);
        $(this).on('keyup', function (e) {
            var parent = $($(this).parent());

            if (e.keyCode === 8 || e.keyCode === 37) {
                var prev = parent.find('input#' + $(this).data('previous'));

                if (prev.length) {
                    $(prev).select();
                }
            } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                var next = parent.find('input#' + $(this).data('next'));

                if (next.length) {
                    $(next).select();
                } else {
                    if (parent.data('autosubmit')) {
                        parent.submit();
                    }
                }
            }
        });
    });
</script>
</body>

</html>
