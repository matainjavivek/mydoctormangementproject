
@extends('partials.user.header')

@section('headerNone')
<div class="top-header display-none">
@endsection

@section('front_title')
    {{ __('Login') }}
@endsection

@section('headerNoneEnd')
</div>
@endsection
@section('content')
<style>
    @media screen and (max-width: 576px) {
#whatsapp_chat_widget{
    display: none !important
}};
</style>
    <div class="mob_inp " id="otp_send">


        <div class="background content_width " >
            <div class="container">
                <div class="row " >
                    <div class="col-sm-6 col-12 p-0">
                        <div class="slider">
                        <a class="sleep_logo ps-3" href="#"><img src="{{ asset('frontend/images/logo.png')}}" alt=""></a>
                        <div id="demo" class="carousel slide" data-bs-ride="carousel">

                        <!-- Indicators/dots -->
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
                            <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
                            <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
                        </div>

                        <!-- The slideshow/carousel -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                            <img src="{{ asset('frontend/images/slide_img_11.png')}}" alt="Los Angeles" class="d-block" style="width:100%">
                            <div class="carousel-caption">
                                <div class="slide_text">Mental health conditions, such as depression or insomnia are real.</div>
                            </div>
                            </div>
                            <div class="carousel-item">
                            <img src="{{ asset('frontend/images/slide_img_22.png')}}" alt="Chicago" class="d-block" style="width:100%">
                            <div class="carousel-caption">
                                <h3>Chicago</h3>
                                <p>Thank you, Chicago!</p>
                            </div>
                            </div>
                            <div class="carousel-item">
                            <img src="{{ asset('frontend/images/slide_img_33.png')}}" alt="New York" class="d-block" style="width:100%">
                            <div class="carousel-caption">
                                <h3>New York</h3>
                                <p>We love the Big Apple!</p>
                            </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-12 my-auto">
                        <div class="mob_num_part px-3">

                            <h1 class="py-4 fs_18">Let’s get started! Enter your mobile number</h1>
                            <div class="alert alert-danger" id="showError" style="display: none;"></div>
                            <!-- <form action="{{route('front.signup')}}" method="POST">
                                @csrf -->
                                <div class="input-group fildStyle">
                                <span class="input-text" >
                                    <select class="form-select border-0 rounded-0" name="country_code" id="country_code">
                                        <option value="+62">+62</option>
                                        <option value="+91">+91</option>
                                    </select>

                                </span>
                                <input type="tel" maxlength="10" minlength="10"  class="form-control border-0 rounded-0" name="phone" id="phone" placeholder="Mobile Number">

                                </div>
                                <span class="gu-hide" style="color: red;" id="errorphone">{{__('validation.Phone number_required')}}</span>
                                <div class="alert alert-danger" id="showError" style="display: none;"></div>
                                <div class="form-group">
                                    <div id="recaptcha-container"></div>
                                </div>
                           <div class="text-center">

                                <button type="submit"  id="login_submit" class="continue_btn submit_btn " onclick="sendFirebasePhoneOTP();">Submit</button>


                            <div class="py-4 text-center"><a class="trouble_btn" href="#">Trouble signing in?</a></div>
                            {{-- </form> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>


    <div class="daftar gu-hide" id="otp_receive">

        <div class="background">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-9 col-sm-12 mx-auto">
                        <div class="daftar-contact-wrap">
                            <div class="daftar-contact">
                                <div class="">
                                    <div class="mb-4">
                                        <div class="row">
                                            <div class="col">
                                                <div class="text-start display-block">
                                                    <i class="fa fa-angle-left "></i>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="text-end">
                                                    <a class="help_btn" href="#"><i class="fa-regular fa-circle-question"></i> Help</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="my-auto">
                                            <div class="login_part p-3 p0">
                                                 <div class="alert alert-danger" id="showError_verify" style="display: none;"></div>
                                                <div class="fs_18">Enter the 6-digit OTP sent to</div>
                                                <div class="pb-4 fs_18" id="phone_number"></div>
                                                <div class="otp-box">
                                                    <form  class="digit-group" data-group-name="digits" data-autosubmit="false" autocomplete="on">
                                                        <input type="text" id="digit-1" name="digit-1" data-next="digit-2" maxlength="1">
                                                        <input type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" maxlength="1">
                                                        <input type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" maxlength="1">
                                                        <input type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" maxlength="1">
                                                        <input type="text" id="digit-5" name="digit-5" data-next="digit-6" data-previous="digit-4" maxlength="1">
                                                        <input type="text" id="digit-6" name="digit-6" data-previous="digit-5" maxlength="1">
                                                    </form>
                                                </div>
                                                <div class="code_recv pt-3">
                                                    {{-- <div class="">Didn’t receive the code?</div>
                                                    <a href="#" class="resend_btn ms-3">Resend</a> --}}
                                                </div>
                                                {{-- <div class="pt-2 pb-5"><a class="trouble_btn" href="#">Get OTP on
                                                        call</a>
                                                    </div> --}}
                                                {{-- <a  onclick="verifyPhoneNumber()"> --}}
                                                <div class="text-center">
                                                    <button type="submit" id="verify_button" class="continue_btn submit_btn " onclick="verifyPhoneNumber()">Verify</button>
                                                </div>
                                                {{-- <div class="continue_btn_wrap">
                                                    <button class="continue_btn" onclick="verifyPhoneNumber()">Continue</button>
                                                </div> --}}
                                                {{-- </a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}" />



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>

<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->

<script src="{{ asset('frontend/js/custom.js') }}"></script>


</body>
</html>
@endsection
