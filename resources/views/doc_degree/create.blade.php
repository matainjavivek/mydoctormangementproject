<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" >
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('degree.store') }}" onsubmit="return validDegree()" name="myForm"  enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-12 form-group">
         
                  
            
            <div>
                <label class="form-control-label" for="name">{{__('messages.Name')}}</label>
                <input type="text" class="form-control" id="name" name="name" required/>
               <span class="gu-hide" style="color: red;" id="errorname">{{__('validation.Name_required')}}</span>
                <span class="text-danger">
                <!-- @error('name')
                {{$message}}
                @enderror  
                </span>
                </div> -->

                <div>
                <label class="form-control-label" for="description">{{__('messages.Description')}}</label>
                <input type="text" class="form-control" id="description" name="description" required />
                 <span class="gu-hide" style="color: red;" id="errordescription">{{__('validation.description_required')}}</span> 
                
                <!-- <span class="text-danger">
                @error('description')
                {{$message}}
                @enderror  
                </span> -->
            </div>

                <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>
</div>
