@extends('layouts.admin')

@section('title')
    {{ __('Manage Degree') }}
@endsection

@section('action-button')
    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('Create Degree')}}" data-url="{{route('degree.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div>


    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('Id')}}</th>
                                <th>{{__('messages.Name')}}</th>
                                <th>{{__('messages.Description')}}</th>
                                <th width="200px">{{__('is_Active')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($degrees))
                            @foreach ($degrees as $doc=>$degree)
                                <tr>
                                <td class="Permission">{{isset($degree['id'])?$degree['id']:''}}</td>
                                    <td class="Permission">{{isset($degree['name'])?$degree['name']:''}}</td>
                                    <td class="Permission">{{isset($degree['description'])?$degree['description']:''}}</td>
                                   
                                    

                                    <td class="Action">
                                        <span>

                                            <a href="#" data-url="{{ URL::to('degree/'.$degree['id'].'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Edit degree')}}" class="edit-icon"><i class="fas fa-pencil-alt" title="Edit"></i></a>

                                            <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$degree['id']}}').submit();"><i class="fas fa-trash" title="Delete"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['degree.destroy', $degree['id']],'id'=>'delete-form-'.$degree['id']]) !!}
                                            {!! Form::close() !!}



                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
