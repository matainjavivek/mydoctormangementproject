

@extends('layouts.admin')


@section('title')
    {{ __('commission.Agent Commission') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <h5>{{isset($commissions[0]->agent->name)?$commissions[0]->agent->name:''}}</h5>
                        
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                                <th>{{__('Clinic Name')}}</th>
                                <th>{{__('Clinic image')}}</th>
                                <th>{{__('commission.Commission')}}</th>


                            </thead>
                            <tbody id='orders'>
                                @php $total=0;@endphp
                                    @if(isset($commissions))
                                    @foreach ($commissions as $commission)
                                    
                                        <tr>
                                            @if(isset($commission->clinic))
                                            <td>{{isset($commission->clinic->name)?$commission->clinic->name:''}}</td>
                                            <td class="Permission"> <img alt="image" class="mr-3 rounded-circle" width="50" height="50" src="@if($commission->clinic->image){{asset('/storage/clinic/'.$commission->clinic->image)}} @else {{asset('assets/img/avatar/avatar-1.png')}} @endif"></td>
                                            @endif
                                            <td>{{isset($commission->commission)?$commission->commission:''}}</td>

                                        </tr>
                                        @php $total=$total+$commission->commission; @endphp
                                    @endforeach
                                    @endif
                                </tbody>
                        </table>
                        <div> Total Commission = <span id="total">{{$total}}</span> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

<script>

   </script>
