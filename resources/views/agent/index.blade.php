@extends('layouts.admin')

@section('title')
    {{ __('messages.Manage Agent') }}
@endsection

@section('action-button')
    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('messages.Create agent')}}" data-url="{{route('agent.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('messages.Name')}}</th>
                                <th>{{__('messages.City')}}</th>
                                <th>{{__('messages.Province')}}</th>
                                <th>{{__('messages.Phone')}}</th>
                                <th>{{__('messages.Referral Code')}}</th>
                                <th>{{__('messages.Url')}}</th>
                                <th>{{__('messages.QR')}}</th>
                                <th>{{__('Wallet')}}</th>
                                <th width="200px">{{__('messages.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($agents as $key=>$agent)

                                <tr>
                                    <td class="Permission">{{isset($agent['name'])?$agent['name']:''}}</td>
                                    <td class="Permission">{{isset($agent['city'])?$agent['city']:''}}</td>
                                    <td class="Permission">{{isset($agent['province'])?$agent['province']:''}}</td>
                                    <td class="Permission">{{isset($agent['phone'])?$agent['phone']:''}}</td>
                                    <td><p class= "code" id="copycode-{{$agent['id']}}">{{isset($agent['referral_code'])?$agent['referral_code']:''}} </p><a class= "button copy-button" href="#" id="copy-{{$agent['id']}}" onclick="CopyToClipboard('{{$agent['id']}}');return false;">Copy</a></td>
                                    <td>
<!--
                                   <p class= "urlcode" id="copycode-{{$agent['short_url']}}"><a href="{{route('url',[$agent['short_url']])}}">
                                        {{isset($agent['short_url'])?$agent['short_url']:''}}</a></p> -->

                                        <!-- <div class= "urlbtn  copy-button"  href="#" data-ajax-popup="true" data-url="{{route('copy',['agent',$agent['referral_code']])}}"  id="copy-{{$agent['short_url']}}" >Copy</div> -->
                                    <!-- copy_url -->
                                        @php
                                        $url="http://" . $_SERVER['HTTP_HOST'].'/order/create/agent/'.$agent['referral_code'];
                                        @endphp
                                        <p  class="copy" type="hidden" id="copy{{$agent['id']}}">{{route('url',[$agent['short_url']])}} </p><a class="copyUrl" href="#"  id="copyUrl-{{$agent['id']}}" onclick="Copy({{$agent['id']}}) ;return false;">copy</a>
                                        <!-- <p class= "copy" id="copy">{{$url}}  </p><a href="#" onclick="Copy('copy');return false;">Copy</a></td> -->



                                    <span class="url" id="url"><a href="https://api.whatsapp.com/send?text={{ $url}}"  target="_blank" ><i class="td-share fas fa-share-alt" title="Share"></i></a></span>
                                    </td>

                                    @php
                                         $image ='Name : '.$agent['name'].', City : '.$agent['city'].', Province : '.$agent['province'].', Phone : '.$agent['phone'].', referral_code : '.$agent['referral_code'];

                                    @endphp
                                    <td >
                                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                                            <a href="#" class="QR" data-ajax-popup="true" data-title="" data-url="{{route('doctor.qrview',[$image,$agent['referral_code']])}}">
                                                 {{__('VIEW')}}
                                            </a>
                                        </div>
                                    </td>
                                    <td class="Permission">{{isset($agent['wallet'])?$agent['wallet']:''}}</td>
                                    <td class="Action">
                                        <span>

                                            <a  data-url="{{ URL::to('agent/'.$agent['referral_code'].'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('messages.Edit agent')}}" class="edit-icon"><i class="fas fa-pencil-alt" title="Edit"></i></a>

                                            <a  class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$agent['referral_code']}}').submit();"><i class="fas fa-trash" title="Delete"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['agent.destroy', $agent['referral_code']],'id'=>'delete-form-'.$agent['referral_code']]) !!}
                                            {!! Form::close() !!}

                                            {{-- <a  class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$agent['referral_code']}}').submit();"><i class="fas fa-trash" ></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['agent.destroy', $agent['referral_code']],'id'=>'delete-form-'.$agent['referral_code']]) !!}
                                            {!! Form::close() !!} --}}

                                            <a href="{{route('agent.commission',[$agent['id']])}}"  class="edit-icon"><i class="fa-solid fa-indian-rupee-sign" title="Commission"></i></a>

                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
