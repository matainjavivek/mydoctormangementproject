<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('agent.store') }}" onsubmit="return validAuthAgent()" name="myForm">
        @csrf
        <div class="row">
            <div class="col-12 form-group">
                <div>
                <label class="form-control-label" for="name">{{__('messages.Name')}}</label>
                <input type="text" class="form-control" id="name" name="name" />
                <span class="gu-hide" style="color: red;" id="errorname">{{__('validation.Name_required')}}</span>
                </div>
                <div>
                <label class="form-control-label" for="city">{{__('messages.City')}}</label>
                <input type="text" class="form-control" id="city" name="city" />
                <span class="gu-hide" style="color: red;" id="errorcity">{{__('validation.City_required')}}</span>
                </div>
                <div>
                <label class="form-control-label" for="province">{{__('messages.Province')}}</label>
                <input type="text" class="form-control" id="province" name="province"/>
                <span class="gu-hide" style="color: red;" id="errorprovince">{{__('validation.Province_required')}}</span>
                <div>
                <label class="form-control-label" for="phone">{{__('messages.Phone')}}</label>
                <input type="number" class="form-control" id="phone" name="phone"   />
                <span class="gu-hide" style="color: red;" id="errorphone">{{__('validation.Phone_number_required')}}</span>
                </div>
                <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>
</div>
