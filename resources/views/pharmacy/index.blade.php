@extends('layouts.admin')


@section('title')
    {{ __('messages.Manage Pharmacy') }}
@endsection

@section('action-button')

    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('messages.Create Pharmacy')}}" data-url="{{route('pharmacy.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div>

    </div>
@endsection

@section('content')
{{-- @php
    dd($pharmacys);
@endphp --}}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('messages.Name')}}</th>
                                <th>{{__('messages.City')}}</th>
                                <th>{{__('messages.Province')}}</th>
                                <th>{{__('messages.Phone')}}</th>
                                <th>{{__('messages.Referral Code')}}</th>
                                <th>{{__('messages.Url')}}</th>
                                <th>{{__('messages.QR')}}</th>
                                <th width="200px">{{__('messages.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($pharmacys))
                            @foreach ($pharmacys as $key=>$pharmacy)

                                <tr>
                                    <td class="Permission">{{isset($pharmacy['name'])?$pharmacy['name']:''}}</td>
                                    <td class="Permission">{{isset($pharmacy['city'])?$pharmacy['city']:''}}</td>

                                    <td class="Permission">{{isset($pharmacy['province'])?$pharmacy['province']:''}}</td>
                                    <td class="Permission">{{isset($pharmacy['phone'])?$pharmacy['phone']:''}}</td>
                                    <td><p class= "code" id="copycode-{{$pharmacy['id']}}">{{isset($pharmacy['referral_code'])?$pharmacy['referral_code']:''}} </p><a class= "button copy-button" href="#" id="copy-{{$pharmacy['id']}}" onclick="CopyToClipboard('{{$pharmacy['id']}}');return false;">Copy</a></td>
                                    <td class="Permission">
                                    {{-- <!-- <p class= "urlcode" id="copycode-{{$pharmacy['short_url']}}">
                                    <a href="{{route('url',[$pharmacy['short_url']])}}">
                                        {{isset($pharmacy['short_url'])?$pharmacy['short_url']:''}}
                                    </a></p> --> --}}

                                    {{-- <!-- <div class= "urlbtn  copy-button"  href="#" data-ajax-popup="true" data-url="{{route('copy',['pharmacy',$pharmacy['referral_code']])}}"  id="copy-{{$pharmacy['short_url']}}" >Copy</div> --> --}}
                                    @php
                                    $url="http://" . $_SERVER['HTTP_HOST']."/order/create/pharmacy/".$pharmacy['referral_code'];
                                    @endphp
                                    <p  class="copy" type="hidden" id="copy{{$pharmacy['id']}}">{{route('url',[$pharmacy['short_url']])}} </p><a class="copyUrl" href="#"  id="copyUrl-{{$pharmacy['id']}}" onclick="Copy({{$pharmacy['id']}}) ;return false;">copy</a>



                                    <span class="url" id="url"><a href="https://api.whatsapp.com/send?text={{ $url}}"><i class="td-share fas fa-share-alt" title="Share"></i></a></span>




                                    </th>



                                    </td>

                                    @php
                                         $image ='Name : '.$pharmacy['name'].', City : '.$pharmacy['city'].', Province : '.$pharmacy['province'].', Phone : '.$pharmacy['phone'].', referral_code : '.$pharmacy['referral_code'];

                                    @endphp
                                    <td >

                                        <a href="#" class="QR" data-ajax-popup="true" data-title="" data-url="{{route('pharmacy.qrview',[$image,isset($pharmacy['referral_code'])?$pharmacy['referral_code']:'P'])}}">
                                                {{__('VIEW')}}
                                        </a>

                                    </td>

                                    {{-- <td class="Action">
                                        <span>

                                            <a href="#" data-url="{{ URL::to('pharmacy/'.$pharmacy['id'].'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Edit Pharmacy')}}" class="edit-icon"><i class="fas fa-pencil-alt"></i></a>

                                            <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$pharmacy['id']}}').submit();"><i class="fas fa-trash"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['pharmacy.destroy', $pharmacy['id']],'id'=>'delete-form-'.$pharmacy['id']]) !!}
                                            {!! Form::close() !!}

                                            <a href="#" data-url="{{ URL::to('order/commission/'.$pharmacy['referral_code']) }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Commission')}}" class="edit-icon"><i class="fa-solid fa-indian-rupee-sign"></i></a>


                                            <a href="#" data-url="{{ URL::to('order/commission/'.$agent['referral_code']) }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Commission')}}" class="edit-icon"><i class="fa-solid fa-indian-rupee-sign"></i></a>
                                        </span>
                                    </td> --}}
                                    <td class="Action">
                                        <span>

                                            <a href="#" data-url="{{ URL::to('pharmacy/'.$pharmacy['referral_code'].'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('messages.Edit Pharmacy')}}" class="edit-icon"><i class="fas fa-pencil-alt" title="Edit"></i></a>

                                            <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$pharmacy['referral_code']}}').submit();"><i class="fas fa-trash" title="Delete"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['pharmacy.destroy', isset($pharmacy['referral_code'])?$pharmacy['referral_code']:'p'],'id'=>'delete-form-'.$pharmacy['referral_code']]) !!}
                                            {!! Form::close() !!}

                                            <a href="{{route('order.commission',[isset($pharmacy['referral_code'])?$pharmacy['referral_code']:'P','pharmacy'])}}" class="edit-icon"><i class="fa-solid fa-indian-rupee-sign" title="Commission" ></i></a>

                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
