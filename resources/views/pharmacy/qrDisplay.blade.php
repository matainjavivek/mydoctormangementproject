{{-- <div class="card bg-none card-box">


        @php
            $image =QrCode::format('png')->size(300)->generate($img);

        @endphp
        <div class="row">
            <div class="col-6 form-group">
                {{$image}}
                <p>{{$referral_code}}</p>
                <a href="{{route('pharmacy.download',[$img,$referral_code])}}">Download Now</a>
            </div>


            <div class="form-group col-12 text-right">
            <a href="/images/myw3schoolsimage.jpg" download>
                <input type="submit" value="{{__('Downlode')}}" class="btn-create badge-blue"></a>
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>

</div> --}}



<div class="container_part m-auto">
  <div class="logo p-5">
    <div class="white"></div>
    <a class="pic" href="#"><img src="{{ asset('assets/images/logo-full.png') }}" alt=""></a>
  </div>
  <div class="m-4">
  <div class="box pt-2 text-center">
    <h2 class="py-4 text-warning"><p>{{$pharmacy->name}}</p></h2>
    <h3>Scan Code</h3>
    @php
        $image =QrCode::size(300)->generate($img);
        // format('png')->
    @endphp
    {{$image}}
   
    <img src="{{ asset('assets/images/Group 1.png') }}" alt="">
   
    <h3 class="pt-5">Referral Code : <p>{{$pharmacy->referral_code}}</p></h3>
    
  </div>
</div>
</div>

<link rel="stylesheet" href="{{ asset('assets/css/qr.css') }}">
