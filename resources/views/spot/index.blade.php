@extends('layouts.admin')

@section('title')
    {{ __('Spot') }}
@endsection

 @section('action-button')
    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('Create Spot')}}" data-url="{{route('spot.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div> 


    </div>
@endsection 

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('Id')}}</th>
                                <th>{{__('name')}}</th>
                                <th>{{__('position')}}</th>
                             
                                <th width="200px">{{__('Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($spots))
                            @foreach ($spots as $map=>$spot)
                                <tr>
                                <td class="Permission">{{isset($spot['id'])?$spot['id']:''}}</td>
                                    <td class="Permission">{{isset($spot['name'])?$spot['name']:''}}</td>
                                   <td>{{$spot->position}}</td>
                                  
                                   </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
