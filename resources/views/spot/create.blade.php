<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" >
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('spot.store') }}" onsubmit="return validZone()" name="myForm"  enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-12 form-group">
         
                  
            
            <div>
                <label class="form-control-label" for="spot">Spot</label>
                <input type="text" class="form-control"  name="name" placeholder="spot" />
                <span class="gu-hide" style="color: red;" > </span>
                </div>


                <label class="form-control-label" for="spot">Position</label>
                    <input type="text"  class="form-control"  name='position'/>
                </div>
                
                <div class="mb-3">
    <label for="exampleInputCheckAdmin" class="form-label">Select Spot Position</label>
    <select name="position" id="position" class="form-control">
      <option value="">Select Position</option>
      <option value="kolkata">Superadmin</option>
      <option value="Delhi">Admin</option>
      <option value="Hydrabad">User</option>
       
    </select>

  </div>
                
                <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAwQ3Gm7MIQ61LSG4u2s6XBuZyLZMj47CE&libraries=drawing" async defer></script>
    <script src="drawmap.js"></script>
</div>
