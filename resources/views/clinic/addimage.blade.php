{{-- <div class="row"> 
        <div class="col-12">
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade " id="business-setting" role="tabpanel" aria-labelledby="profile-tab3"> </div>
                <div class="tab-pane fade fade show active" id="system-setting" role="tabpanel" aria-labelledby="profile-tab3">
                    <form method="POST"  action="{{ route('clinic.image.update') }}" name="myForm" class="date-form" id="contact_form">
                        @csrf
                        <input type="hidden" name="doctor_id" value="{{isset($id)?$id:''}}">
                        <div class="card bg-none">
                            <div class="row company-setting">
                                <div class="col-10 form-group">
                                    
                                <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                        <div class="choose-file">
                            <label for="image">
                                <div>{{__('Choose file here')}}</div>
                                <input class="form-control" name="image" type="file" id="image" accept="image/*" data-filename="profile_update">
                            </label>
                            <p class="profile_update"></p>
                        </div>
                        @error('avatar')
                        <span class="invalid-feedback text-danger text-xs" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <span class="clearfix"></span>
                    <span class="text-xs text-muted">{{ __('Please upload a valid image file. Size of image should not be more than 2MB.')}}</span>
                </div>

                                </div>
                                <div class="form-group col-md-2 text-left" style="padding-top: 22px;">
                                    <input type="submit" id="save-btn" value="{{__('Save')}}" class="btn-create badge-blue">
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>--}}



    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('clinic.image.update') }}"  name="myForm" enctype="multipart/form-data">
        
        @csrf
        <div class="row">
            <div class="col-12 form-group">
                <input type="hidden" name="id" value="{{$id}}">
                {{-- <div class="form-group">
                    <input type="file" id="image" name="image" class="@error('image') is-invalid @enderror form-control">
                    <label for="exampleInputEmail1">Please Select Image</label>
                    
                    @error('image')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                </div> --}}

                <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                        <div class="choose-file">
                            <label for="image">
                                <div>{{__('Choose file here')}}</div>
                                <input class="form-control" name="image" type="file" id="image" accept="image/*" data-filename="profile_update">
                            </label>
                            <p class="profile_update"></p>
                        </div>
                        @error('avatar')
                        <span class="invalid-feedback text-danger text-xs" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <span class="clearfix"></span>
                    <span class="text-xs text-muted">{{ __('Please upload a valid image file. Size of image should not be more than 2MB.')}}</span>
                </div>

               
                <input type="submit" value="{{__('Save')}}"  class="btn-create badge-blue">
                {{-- <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal"> --}}
            </div>
            
        </div>
    </form>
</div>
<script src="{{asset('assets/js/user.js')}}"></script>
