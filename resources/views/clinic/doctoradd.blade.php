<div class="row">
        <div class="col-12">

            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade " id="business-setting" role="tabpanel" aria-labelledby="profile-tab3"> </div>
               
                <div class="tab-pane fade fade show active" id="system-setting" role="tabpanel" aria-labelledby="profile-tab3">
                  
                    <form method="POST"  action="{{ route('clinic.timetadle.add') }}" name="myForm" class="date-form" id="contact_form">
                        @csrf
                        <input type="hidden" name="clinic_id" value="{{isset($id)?$id:''}}">
                        <input type="hidden" name="total_doctor" id="total_doctor" value="0">
                        <div class="card bg-none">
                              {{-- 
                              <div class="col-md-12 text-right" onclick="timestame({{$doctor}})"><i class="fas fa-plus"></i></div> --}}
                            <div class="row company-setting" id="add_doctor">
                                {{-- <div class="col-12"> --}}
                                    <div class="col-10 form-group">
                                        {{ Form::label('doctor', __('Doctor'),['class'=>'form-control-label']) }}
                                        {{-- {{ Form::select('doctor[]', $doctors->pluck('name','id'),null, array('class' => 'form-control select2','required'=>'required')) }} --}}
                                        {{ Form::select('doctor[]', $doctors->pluck('name','id'),null, array('class' => 'form-control select2','multiple'=>'','required'=>'required')) }}
                                    </div>
                                    <div class="form-group col-md-2 text-left" style="padding-top: 22px;">
                                          <input type="submit" id="save-btn" value="{{__('Save')}}" class="btn-create badge-blue">
                                    </div> 
                            </div>
                            <script> var i=0; </script>
                           
                            
                        </div>

                    </form>

                </div>
            </div>
        </div>
       
    </div>