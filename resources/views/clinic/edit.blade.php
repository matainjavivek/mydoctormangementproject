<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" >
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('clinic.update') }}" onsubmit="return validChamber()" name="myForm" >
        @csrf
        <input type="hidden" name="id" id="id" value="{{$clinics->id}}"/>
        <div class="row">
            <div class="col-12 form-group">
                <div>
                    <label class="form-control-label" for="shop_name">{{__('messages.shop_name')}}</label>
                    <input type="text" class="form-control" id="shop_name" name="shop_name" value="{{$clinics->name}}" />
                    <span class="gu-hide" style="color: red;" id="errorshop_name">{{__('validation.shop_name_required')}}</span>
                </div>
                {{-- <div>
                    <label class="form-control-label" for="email">{{__('messages.email')}}</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{$clinics->email}}" />
                    <span class="gu-hide" style="color: red;" id="erroremail">{{__('validation.E-Mail Address_required')}}</span>
                </div> --}}
                <div>
                    <label class="form-control-label" for="phone">{{__('messages.mobile_number')}}</label>
                    <input type="number" class="form-control" id="phone" name="phone" value="{{$clinics->phone}}"/>
                    <span class="gu-hide" style="color: red;" id="errorphone">{{__('validation.number_required')}}</span>
                </div>

                <div>
                    <label class="form-control-label" for="shop_phone">{{__('messages.shop_phone_number')}}</label>
                    <input type="number" class="form-control" id="shop_phone" name="shop_phone" value="{{$clinics->shop_phone}}"/>
                    <span class="gu-hide" style="color: red;" id="errorshop_phone">{{__('validation.shop_phone_number_required')}}</span>
                </div>

                 {{-- <div class="col-12 form-group">
                     {{ Form::label('status', __('Status'),['class'=>'form-control-label']) }}
                     {{ Form::select('status', \App\Models\User::$statues,null, array('class' => 'form-control select2','required'=>'required')) }}
                </div> --}}

                
                 <div class="col-12 form-group">
                                    <label class="form-control-label">{{__('messages.Status')}} *</label>
                                    <select name="status" class="form-control select2" id="status" value="{{isset($clinics['status'])?$clinics['status']:''}}">
                                    
                                        @If(!is_null($clinics['status']))
                                         <option value="{{isset($clinics['status'])?$clinics['status']:''}}">{{__(\App\Models\User::$statues[$clinics['status']]) }}</option>
                                        @else
                                        <option value="">{{__('messages.Select status')}}</option>
                                        @endif
                                        @if($clinics['status'] ==0)
                                        <option value="1">{{__('verified')}}</option>
                                        @endif
                                        @if($clinics['status'] ==1)
                                        <option value="0">{{__('not verified')}}</option>
                                         @endif
                                    </select>
                </div> 

      

                <div>
                <label class="form-control-label" for="address">{{__('messages.Address')}}</label>
                <input type="text" class="form-control" id="address" name="address" value="{{$clinics->address}}"/>
                <span class="gu-hide" style="color: red;" id="erroraddress">{{__('validation.address_required')}}</span>
                </div>


                <div id="map" style="width: 100%; height: 400px;top:4px"></div>
                    <ul class="geo-data">
                        <li>Full Address: <span id="location"></span></li>
                        <li>Country: <span id="country"></span></li>
                        <li>Postal Code: <span id="postal_code"></span></li>
                        <li>Latitude: <span id="lat"></span></li>
                        <li>Longitude: <span id="lon"></span></li>

                    </ul>
                    <input type="hidden" id="chamber_lat" name='lat'/>
                    <input type="hidden" id="chamber_lon" name='lon'/>
                    <input type="hidden" id="postal_code1" name='postal_code'/>
                </div>

                <input style="margin-right: 10px;" type="submit" value="{{__('Update')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>
    
</div>

