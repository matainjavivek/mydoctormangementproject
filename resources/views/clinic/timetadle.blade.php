<div class="row">
        <div class="col-12">

            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade " id="business-setting" role="tabpanel" aria-labelledby="profile-tab3"> </div>
               
                <div class="tab-pane fade fade show active" id="system-setting" role="tabpanel" aria-labelledby="profile-tab3">
                  
                    <form method="POST"  action="{{ route('clinic.doctor.schedule.store') }}" name="myForm" class="date-form" id="contact_form">
                        @csrf
                        <input type="hidden" name="clinic_id" value="{{isset($clinic_id)?$clinic_id:''}}">
                        <input type="hidden" name="doctor_id" value="{{isset($doctor_id)?$doctor_id:''}}">
                        <input type="hidden" name="total_schedule" id="total_doctor" value="0">
                        <div class="card bg-none">
                              
                              <div class="col-md-12 text-right" onclick="timestame()"><i class="fas fa-plus"></i></div>
                            <div class="row company-setting" id="add_doctor">
                                {{-- <div class="col-12"> --}}
                                    
                                      <div class="col-6 form-group">
                                          {{ Form::label('day', __('Day'),['class'=>'form-control-label']) }}
                                          {{ Form::select('day_0', $days,null, array('class' => 'form-control select2','required'=>'required')) }}
                                      </div>
                                      <div class="col-3 form-group">
                                          {{ Form::label('Start', __('Start Time'),['class'=>'form-control-label']) }}
                                          {{ Form::select('start_times_0', $times,null, array('class' => 'form-control select2','required'=>'required')) }}
                                      </div>
                                      <div class="col-3 form-group">
                                          {{ Form::label('end_times_0', __('End Time'),['class'=>'form-control-label']) }}
                                          {{ Form::select('end_times_0', $times,null, array('class' => 'form-control select2','required'=>'required')) }}
                                      </div>
                                      

                                      
                            </div>
                            <script> var i=0; </script>
                            <div class="form-group col-md-2 text-left" style="padding-top: 22px;">
                              <input type="submit" id="save-btn" value="{{__('Save')}}" class="btn-create badge-blue">
                          </div>
                            <div class="card col-12">
                                <table class="table align-items-center mb-0">
                                    <thead>
                                        <tr>
                                            
                                            <th>{{__('Day')}}</th>
                                            <th>{{__('Visiting Hours')}}</th>
                                            <th>{{__('messages.Action')}}</th>
                                        </tr>
                                        </thead>
                                    <tbody class="list">
                                        @if($doctor_timetadles)
                                            {{-- @foreach($doctor_timetadles as $key =>$doctor_timetadle) --}}
                                            @foreach($doctor_timetadles as $key =>$doctor)
                                            
                                                <tr>
                                                

                                                <td class="col-6">{{isset($doctor->day)?$doctor->day:""}}</td>

                                                <td class="col-6">{{isset($doctor->start_time)?date('g:i a', strtotime($doctor->start_time)):""}} - {{isset($doctor->end_time)?date('g:i a', strtotime($doctor->end_time)):""}}
                                                </td>

                                                <td class="col-2">
                                                <a href="{{ route('clinic.schedule.destroy',[$doctor->id])}}" class="delete-icon mx-2"  ><i class="fas fa-trash" title="Delete"></i></a>
                                                
                                                </td>
                                                </tr>
                                            @endforeach
                                            {{-- @endforeach --}}
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
       
    </div>