@extends('layouts.admin')

@section('title')
    {{$clinic->name}}
@endsection

@section('action-button')
    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('Add doctor')}}" data-url="{{route('clinic.timetadle.doctor',[$id])}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div>


    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                              <tr>
                                    <th>{{__('Image')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('messages.Action')}}</th>
                              </tr>
                            </thead>
                            <tbody>
                              @if($ClinicDoctors)
                              {{-- @foreach($doctor_timetadles as $key =>$doctor_timetadle) --}}
                              @foreach($ClinicDoctors as $key =>$doctor)
                              
                                  <tr>
                                    <td class="Permission"> 
                                     <img alt="image" class="mr-3 rounded-circle" width="50" height="50" src="@if($doctor->doctorName)@if($doctor->doctorName->image){{asset('/storage/doctor/'.$doctor->doctorName->image)}} @else {{asset('assets/img/avatar/avatar-1.png')}}@endif @endif">
                                    </td>
                                  <td class="col-4">
                                     {{isset($doctor->doctorName->name)?$doctor->doctorName->name:""}}
                                  </td>

                                  {{-- <td class="col-6">{{isset($doctor->day)?$doctor->day:""}}</td>

                                  <td class="col-6">{{isset($doctor->start_time)?$doctor->start_time:""}} - {{isset($doctor->end_time)?$doctor->end_time:""}}
                                  </td>--}}

                                  <td class="col-2">
                                     <a href="{{ route('clinic.doctor.destroy',[$doctor->id])}}" class="delete-icon mx-2"  ><i class="fas fa-trash" title="Delete"></i></a>

                                     <a href="#" data-url="{{route('clinic.doctor.schedule',[$clinic->id,$doctor->doctor_id])}}" data-size="lg" data-ajax-popup="true" data-title="Edit schedule of {{isset($doctor->doctorName->name)?$doctor->doctorName->name:''}}" class="edit-icon"><i class="fa-solid fa-hospital-user"></i></a>
                                  </td> 
                                  </tr>
                              @endforeach
                              {{-- @endforeach --}}
                          @endif
                            </tbody>
                        </table>
                        {{-- <table class="table align-items-center mb-0">
                              <thead>
                                  <tr>
                                      <th>{{__('Name')}}</th>
                                      <th>{{__('Day')}}</th>
                                      <th>{{__('Visiting Hours')}}</th>
                                      <th>{{__('messages.Action')}}</th>
                                  </tr>
                              </thead>
                              
                              <tbody class="list">
                                  @if($doctor_timetadles)
                                      @foreach($doctor_timetadles as $key =>$doctor_timetadle)
                                      @foreach($doctor_timetadle as $key =>$doctor)
                                      
                                          <tr>
                                          <td class="col-4">{{isset($doctor->doctorName->name)?$doctor->doctorName->name:""}}</td>

                                          <td class="col-6">{{isset($doctor->day)?$doctor->day:""}}</td>

                                          <td class="col-6">{{isset($doctor->start_time)?$doctor->start_time:""}} - {{isset($doctor->end_time)?$doctor->end_time:""}}
                                          </td>

                                          <td class="col-2">
                                          <a href="{{ route('clinic.timetadle.destroy',[$doctor->id])}}" class="delete-icon mx-2"  ><i class="fas fa-trash" title="Delete"></i></a>
                                          
                                          </td>
                                          </tr>
                                      @endforeach
                                      @endforeach
                                  @endif
                              </tbody>
                        </table> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
   

@endsection
