<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" >
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('clinic.store') }}" onsubmit="return validChamber()" name="myForm" >
        @csrf

        <div class="row">
            <div class="col-12 form-group">
                <div>
                    <label class="form-control-label" for="shop_name">{{__('messages.shop_name')}}</label>
                    <input type="text" class="form-control" id="shop_name" name="shop_name" placeholder="Enter your shop name" />
                    <span class="gu-hide" style="color: red;" id="errorshop_name">{{__('validation.shop_name_required')}}</span>
                </div>
                <div>
                    <label class="form-control-label" for="email">{{__('messages.email')}}</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter your email" />
                    <span class="gu-hide" style="color: red;" id="erroremail">{{__('validation.E-Mail Address_required')}}</span>
                </div>
                <div>
                    <label class="form-control-label" for="phone">{{__('messages.mobile_number')}}</label>
                    <input type="number" class="form-control" id="phone" name="phone" placeholder="Enter your mobile number"/>
                    <span class="gu-hide" style="color: red;" id="errorphone">{{__('validation.number_required')}}</span>
                </div>
                
                <div>
                    <label class="form-control-label" for="shop_phone">{{__('messages.shop_phone_number')}}</label>
                    <input type="number" class="form-control" id="shop_phone" name="shop_phone" placeholder="Enter your shop phone number"/>
                    <span class="gu-hide" style="color: red;" id="errorshop_phone">{{__('validation.shop_phone_number_required')}}</span>
                </div>

                <div>
                    <label class="form-control-label" for="phone">{{__('Referral Code')}}</label>
                    <input type="text" class="form-control" id="referral_code" name="referral_code" placeholder="Enter referral code"/>
                    <span class="gu-hide" style="color: red;" id="errorphone">{{__('validation.number_required')}}</span>
                    <span class="gu-hide" style="color: red;" id="errorreferral_code">Enter Valid Code</span>
                    <div id="referral_code_error_text" class="form-text error-text gu-hide">Please enter the correct
                        referral code</div>
                    <div class="referrer-name">Referrer name :-   <span id="user-name"></span> </div>
                </div>
                {{-- <div>
                    <label class="form-control-label" for="password">{{__('Password')}}</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="**********" />
                    <span class="gu-hide" style="color: red;" id="errorpassword">{{__('validation.password_required')}}</span>
                </div> --}}

                <div>
                <label class="form-control-label" for="address">{{__('messages.Address')}}</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="Enter your shop address" />
                <span class="gu-hide" style="color: red;" id="erroraddress">{{__('validation.address_required')}}</span>
                </div>


                <div id="map" style="width: 100%; height: 400px;top:4px"></div>
                    <ul class="geo-data">
                        <li>Full Address: <span id="location"></span></li>
                        <li>Country: <span id="country"></span></li>
                        <li>Postal Code: <span id="postal_code"></span></li>
                        <li>Latitude: <span id="lat"></span></li>
                        <li>Longitude: <span id="lon"></span></li>

                    </ul>
                    <input type="hidden" id="chamber_lat" name='lat'/>
                    <input type="hidden" id="chamber_lon" name='lon'/>
                    <input type="hidden" id="postal_code1" name='postal_code'/>
                </div>

                <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>
    
</div>
