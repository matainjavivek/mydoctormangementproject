@extends('layouts.admin')

@section('title')
    {{__('Clinic Detail')}}
@endsection

@section('action-button')


@endsection

@section('content')
    <div class="card">
     
        <div class="invoice-title">
            {{(isset($clinic['referral_code'])?'#'.$clinic['referral_code']:'')}}
            <div class="float-right mt-3">
             </div>
        </div>

         <div class="invoice-title">
            <h6>{{__('Clinic')}} :</h6>
                         {{(isset($clinic['name'])?$clinic['name']:'')}}
            <div class="address-detail text-right float-right">
            <h6>{{__('Contact No')}} :</h6>
                            
                {{(isset($clinic['shop_phone'])?$clinic['shop_phone']:'')}} <br>
                {{(isset($clinic['email'])?$clinic['email']:'')}}
                </div>
                <div class="float-right mt-3">
                </div>
        </div>



   

     

        <div class="status-section">
                <div class="row">
                    {{-- <div class="col-md-2 col-sm-1 col-1">
                        <div class="text-status">
                            <strong>{{__('Status')}} :</strong>
                                <span class="badge badge-pill badge-info">  {{isset($orders['status'])?$orders['status']:''}}</span>
                        </div>
                    </div> --}}
                    @if(isset($clinic->clinicAgent)) 

                    
                    
                    <div class="col-md-3 col-sm-6 col-6">
                        <div class="text-status text-right">{{__('Referral type')}} :<strong>{{isset($clinic->clinicAgent)?$clinic->clinicAgent->agent->type:''}}</strong></div>
                    </div> 
                    <div class="col-md-3 col-sm-6 col-6">
                        <div class="text-status text-right">{{__('Referral name')}} :<strong>{{isset($clinic->clinicAgent)?$clinic->clinicAgent->agent->name:''}}</strong></div>
                    </div>
                     @else
                     <div class="col-md-3 col-sm-6 col-6">
                   
                   <div class="text-status text-right">{{__('Referral details')}} :<strong>Tidak ada referensi</strong></div>
                    </div>
                    @endif
                </div>
            </div>

           


            <div class="row">
                <div class="col-md-12">
                    <div class="justify-content-between align-items-center d-flex">
                        <h4 class="h4 font-weight-400 float-left">{{__('Address')}}</h4>
                     
                    </div>
                    <div class="card">
                        <div class="table-responsive order-table">
                            {{isset($clinic->address)?$clinic->address:''}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
                              
@endsection 