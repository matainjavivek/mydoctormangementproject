@extends('layouts.admin')

@section('title')
    {{ __('messages.Manage Clinic') }}
@endsection

@section('action-button')
    {{-- <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('messages.Create clinic')}}" data-url="{{route('clinic.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div>


    </div> --}}
    <div class="all-button-box row d-flex justify-content-end">

        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
            <a href="{{route('clinic.create')}}" class="btn btn-xs btn-white btn-icon-only width-auto" >
                <i class="fas fa-plus"></i> {{__('Add')}}
            </a>
        </div>
    
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('messages.shop_name')}}</th>
                                {{-- <th>{{__('messages.email')}}</th> --}}
                                <th>{{__('messages.mobile_number')}}</th>
                                <th>{{__('messages.shop_phone_number')}}</th>
                                
                                <th>{{__('messages.Address')}}</th>
                                <th>{{__('Image')}}</th>
                                 <th>{{__('messages.Status')}}</th>
                                <th width="200px">{{__('messages.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($clinics))
                            @foreach ($clinics as $key=>$clinic)
                                <tr>

                                    <td class="Permission">{{isset($clinic['name'])?$clinic['name']:''}}</td>
                                    {{-- <td class="Permission">{{isset($clinic['email'])?$clinic['email']:''}}</td> --}}
                                    <td class="Permission">{{isset($clinic['phone'])?$clinic['phone']:''}}</td>
                                    <td class="Permission">{{isset($clinic['shop_phone'])?$clinic['shop_phone']:''}}</td>
                                    
                                    <td class="Permission">{{isset($clinic['address'])?$clinic['address']:''}}</td>

                                    <td class="Permission"> <img alt="image" class="mr-3 rounded-circle" width="50" height="50" src="@if($clinic['image']){{asset('/storage/clinic/'.$clinic['image'])}} @else {{asset('assets/img/avatar/avatar-1.png')}} @endif"></td>
                                       
                                    
                                        <td>
                                        {{-- {{$clinic->status}} --}}
                                        @if(isset($clinic->status))
                                        @if($clinic->status == 1)
                                            <span class="badge badge-pill badge-primary">{{__(\App\Models\User::$statues[$clinic->status]) }}</span>
                                        @elseif($clinic->status == 0)
                                            <span class="badge badge-pill badge-danger">{{__(\App\Models\User::$statues[$clinic->status]) }}</span>
                                        
                                        @endif
                                         @endif
                                        @if(! isset($clinic->status))
                                              <span class="badge badge-pill badge-primary">{{__(\App\Models\User::$statues[0]) }}</span>  
                                        @endif
                                        {{-- @php
                                        dd(\App\Models\User::$statues[0]);
                                        @endphp --}}
                                    </td>
                                   
                                    <td class="Action">
                                        <span>

                                            <a href="#" data-url="{{ URL::to('clinic/'.$clinic['id'].'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('messages.Edit clinic')}}" class="edit-icon"><i class="fas fa-pencil-alt" title="Edit"></i></a>

                                            <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$clinic['id']}}').submit();"><i class="fas fa-trash" title="Delete"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['clinic.destroy', $clinic['id']],'id'=>'delete-form-'.$clinic['id']]) !!}
                                            {!! Form::close() !!}
                                          <div onclick="doctortimestame()">

                                            <a href="{{route('clinic.schedule',[$clinic['id']])}}" class="edit-icon "><i class="fa-solid fa-hospital-user" title="Doctor Add"></i></a>

                                            <a href="#" data-url="{{route('clinic.image',[$clinic['id']])}}"  data-size="lg" data-ajax-popup="true" data-title="{{__('Add clinic image')}}" class="edit-icon mx-2"> <i class="fa-solid fa-image" title="Image"></i></a>
                                            <a href="{{route('clinic.detail',[$clinic['id']])}}" class="edit-icon bg-warning" data-toggle="tooltip"><i class="fas fa-eye" title="View"></i></a>
                                            </div>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

@endsection
