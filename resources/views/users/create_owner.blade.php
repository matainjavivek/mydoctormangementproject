<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('users.store') }}" onsubmit="return validAuthUsers()" name="myForm">
        @csrf
        <div class="row">
            <div class="col-12 form-group">
                <div>
                <label class="form-control-label" for="name">{{__('messages.Name')}}</label>
                <input type="text" class="form-control" id="name" name="name" />
                <span class="gu-hide" style="color: red;" id="errorname">{{__('validation.Name_required')}}</span>
                </div>
                <div>
                <label class="form-control-label" for="email">{{__('messages.E-Mail Address')}}</label>
                <input type="email" class="form-control" id="email" name="email" />
                <span class="gu-hide" style="color: red;" id="erroremail">{{__('validation.E-Mail Address_required')}}</span>
                </div>
                <div>
                <label class="form-control-label" for="password">{{__('messages.Password')}}</label>
                <input type="text" class="form-control" id="password" name="password" />
                <span class="gu-hide" style="color: red;" id="errorpassword">{{__('validation.Password_required')}}</span>
                </div>
                <div>
                <label class="form-control-label" for="job_title">{{__('messages.Job Title')}}</label>
                <input type="text" class="form-control" id="job_title" name="job_title"/>
                <span class="gu-hide" style="color: red;" id="errorjob_title">{{__('validation.job_title_required')}}</span>
                </div>
                <div>
                <label class="form-control-label" for="role">{{__('messages.Role')}}</label>
                
                <select name="role" class="form-control select2"  id="role" name="role">
                    <option value="">{{__('Select Role')}}</option>
                    @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->name}}</option>
                    @endforeach
                </select>
                <span class="gu-hide" style="color: red;" id="errorrole">{{__('validation.Role_required')}}</span>
            </div>

            @include('custom_fields.formBuilder')

            <div class="form-group col-12 text-right">
                <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>
</div>
