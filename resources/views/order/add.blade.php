@extends('layouts.admin')

@section('title')
    {{ __('messages.Order Create') }}
@endsection

@push('script')
    <script src="{{ asset('assets/js/jscolor.js') }} "></script>
    <script src="{{asset('assets/js/user.js')}}"></script>

@endpush

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade " id="business-setting" role="tabpanel" aria-labelledby="profile-tab3">

                </div>
                <div class="tab-pane fade fade show active" id="system-setting" role="tabpanel" aria-labelledby="profile-tab3">
                <form method="POST"  action="{{ route('order.store') }}" name="myForm" class="date-form" id="contact_form">
                        @csrf
                        <div class="card bg-none">
                            <div class="row company-setting">
                                <div class="col-lg-4 col-md-6 col-sm-6 form-group">
                                    <label class="form-control-label">{{__('messages.Full name')}}</label>
                                    <input type="text" name="name" class="form-controlsystem" id="name" >
                                    <span class="gu-hide" style="color: red;" id="errorname">Name is required</span>
                                    @if ($errors->has('name'))
                                        <span class="invalid feedback"role="alert">
                                            <div style="color: red;">{{ $errors->first('name') }}.</div>
                                        </span>
                                    @endif
                                </div>
                              
                                <div class="col-lg-4 col-md-6 col-sm-6 form-group">
                                    <label class="form-control-label">{{__('messages.Phone')}}</label>
                                    <input type="tel" maxlength="10" minlength="10"  name="mobile" class="form-controlsystem" id="mobile">
                                    <span class="gu-hide" style="color: red;" id="errormobile">Mobile number is required</span>
                                    @if ($errors->has('mobile'))
                                            <span class="invalid feedback"role="alert">
                                                <div style="color: red;">{{ $errors->first('mobile') }}.</div>
                                            </span>
                                    @endif

                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 form-group">
                                    <label class="form-control-label">{{__('messages.Number of tablets')}}</label>
                                    <input type="number" name="tablets" class="form-controlsystem" id="tablets">
                                    <span class="gu-hide" style="color: red;" id="errortablets">Number of tablets is required</span>
                                    @if ($errors->has('tablets'))
                                            <span class="invalid feedback"role="alert">
                                                <div style="color: red;">{{ $errors->first('tablets') }}.</div>
                                            </span>
                                    @endif

                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 form-group">
                                    <label class="form-control-label">{{__('messages.Doctor name')}} *</label>
                                    <input type="text" name="doctor" class="form-controlsystem" id="doctor">
                                    <span class="gu-hide" style="color: red;" id="errordoctor">Doctor name is required</span>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 form-group">
                                    <label class="form-control-label">{{__('Referral code (Optional)')}} </label>
                                    {{-- <input type="text" name="referral_code" class="form-controlsystem" id="referral_code"> --}}
                                    <select name="referral_code" class="form-control select2" id="status" value="{{isset($order['status'])?$order['status']:''}}">
                                        <option value="">{{__('messages.Select referral name')}}</option>
                                        @if (isset($users))
                                        @foreach ($users as $user)
                                        <option value="{{$user->referral_code}}">{{$user->name}} <span>  ({{$user->type}})</span></option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <input class="styled-checkbox" id="checkbox" name="checkbox" type="hidden" value="value1" >
                                <div class="form-group col-md-12 text-right">
                                    <input type="submit" id="save-btn" value="{{__('Save')}}" class="btn-create badge-blue">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- <script>
    $(function(){
     $("#country").select2();
    });
   </script> --}}
