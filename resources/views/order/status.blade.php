
    <div class="row">
        <div class="col-12">

            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade " id="business-setting" role="tabpanel" aria-labelledby="profile-tab3">

                </div>
                <div class="tab-pane fade fade show active" id="system-setting" role="tabpanel" aria-labelledby="profile-tab3">
                <form method="POST" onsubmit="return validateForm()" action="{{ route('order.status.update') }}" name="myForm" class="date-form" id="contact_form">
                        @csrf
                        <input type="hidden" name="order_id" value="{{isset($order['order_id'])?$order['order_id']:''}}">
                        <div class="card bg-none">
                            <div class="row company-setting">
                            <div class="col-6 form-group">
                                <label class="form-control-label" for="role">{{__('messages.Status')}}</label>
                                <select name="status" class="form-control select2" id="status" value="{{isset($order['status'])?$order['status']:''}}">
                                    @If(isset($order['status']))
                                    <option value="{{isset($order['status'])?$order['status']:''}}">{{isset($order['status'])?$order['status']:''}}</option>
                                    @else
                                    <option value="">{{__('messages.Select status')}}</option>
                                    @endif
                                    <option value="Pending">{{__('Pending')}}</option>
                                    <option value="processing">{{__('processing')}}</option>
                                    <option value="Shifted">{{__('Shifted')}}</option>
                                    <option value="Delivered">{{__('Delivered')}}</option>
                                </select>


                                                <!-- <div class="col-lg-4 col-md-6 col-sm-6 form-group">
                                    <label class="form-control-label">{{__('Status')}} *</label>
                                    <input type="text" name="status" class="form-controlsystem" id="status" value="{{isset($order['status'])?$order['status']:''}}">-->

                                </div>

                                <input class="styled-checkbox" id="checkbox" name="checkbox" type="hidden" value="value1" >
                                <div class="form-group col-md-12 text-right">
                                    <input type="submit" id="save-btn" value="{{__('Save Changes')}}" class="btn-create badge-blue">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
