
   @include('order.layout.orderlayout')

    @section('title')
        {{ __('Order') }}
    @endsection
   <body>
      <div class="bg-color">
         <div class="container">
            <div class="row">
               <div class="col-md-4 mx-auto">
                  <div class="text-center pt-4"><img src="{{ asset('assets/images/logo.png') }}" width="45%"></div>
                  <div class="contact">
                     <div class="text-center page-heading">Order Medicine</div>
                     <div class="form-area">
                        <form method="POST" onsubmit="return validateForm()" action="{{ route('order.store') }}" name="myForm" class="date-form" id="contact_form">
                            @csrf

                           <div class="group form-group">
                              <input type="text" class="form-control" id="name" name="name">
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Full name</label>
                              <span class="gu-hide" style="color: red;" id="errorname">Name is required</span>
                           </div>


                            @if ($errors->has('name'))
                                <span class="invalid feedback"role="alert">
                                    <div style="color: red;">{{ $errors->first('name') }}.</div>
                                </span>
                            @endif
                           <div class="group form-group">
                              <input type="number" class="form-control" name="mobile" id="mobile" >
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Mobile phone</label>
                              <span class="gu-hide" style="color: red;" id="errormobile">Mobile number is required</span>
                           </div>
                           @if ($errors->has('mobile'))
                                <span class="invalid feedback"role="alert">
                                    <div style="color: red;">{{ $errors->first('mobile') }}.</div>
                                </span>
                            @endif
                           <div class="group form-group">
                              <input type="number" class="form-control" name="tablets" id="tablets" >
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Number of tablets</label>
                              <span class="gu-hide" style="color: red;" id="errortablets">Number of tablets is required</span>
                           </div>
                           <div class="group form-group">
                              <input type="text" class="form-control" id="doctor" name="doctor">
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Doctor name</label>
                              <span class="gu-hide" style="color: red;" id="errordoctor">Doctor name is required</span>
                           </div>
                           <div class="group form-group marginB">
                              <input type="mail" class="form-control" name="referral_code" id="referral_code" value="{{isset($referral_code)?$referral_code:''}}" >
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Referral code (Optional)</label>
                           </div>
                           <div class="referrer-name">Referrer name :-    {{isset($user['name'])?$user['name']:''}}</div>
                           <div class="small-text">
                                <div class="checkbox-wrap"><input class="styled-checkbox" id="styled-checkbox-1" name="checkbox" type="checkbox"
                                    value="value1" ></div>
                                    <span class="gu-hide" style="color: red;" id="errorstyled-checkbox-1">You need to check box</span>
                                     @if ($errors->has('checkbox'))
                                            <span class="invalid feedback"role="alert">
                                            <div style="color: red;">{{ $errors->first('checkbox') }}.</div>
                                        </span>
                                    @endif
                                <div class="checkbox-text-wrap">I agree to the <a
                                    href="#">Terms and conditions</a></div>
                            </div>
                            <input type="hidden" name="referral_type" value="{{isset($referral_type)?$referral_type:''}}">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary col-md-12">Submit</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
         integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
         crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
         integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
         crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
         integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
         crossorigin="anonymous"></script>
         <script>
        //from validation

            function validateForm() {


                let x = document.forms["myForm"]["name"].value;
                console.log(x);
                if (x == "") {
                    var element = document.getElementById("name");
                    element.classList.add("radbox");
                    var element = document.getElementById("errorname");
                    element.classList.remove("gu-hide");

                    return false;
                }
                if (!/^[a-zA-Z' ']*$/g.test(x)) {
                    var element = document.getElementById("name");
                    element.classList.add("radbox");
                    var element = document.getElementById("errorname");
                    element.classList.remove("gu-hide");
                    document.myForm.name.focus();
                    return false;
                }else{
                    var element = document.getElementById("name");
                    element.classList.remove("radbox");
                    var element = document.getElementById("errorname");
                    element.classList.add("gu-hide");

                }
                let y = document.forms["myForm"]["mobile"].value;
                if (y == "") {
                    var element = document.getElementById("mobile");
                    element.classList.add("radbox");
                    var element = document.getElementById("errormobile");
                    element.classList.remove("gu-hide");
                    //alert("Name must be filled out");
                    return false;
                }else{
                    var element = document.getElementById("mobile");
                    element.classList.remove("radbox");
                    var element = document.getElementById("errormobile");
                    element.classList.add("gu-hide");

                }
                let j = document.forms["myForm"]["tablets"].value;
                if (j == "") {

                    var element = document.getElementById("tablets");
                    element.classList.add("radbox");
                    var element = document.getElementById("errortablets");
                    element.classList.remove("gu-hide");
                    return false;
                }else{
                    var element = document.getElementById("tablets");
                    element.classList.remove("radbox");
                    var element = document.getElementById("errortablets");
                    element.classList.add("gu-hide");

                }
                let k = document.forms["myForm"]["doctor"].value;
                if (k == "") {

                    var element = document.getElementById("doctor");
                    element.classList.add("radbox");
                    var element = document.getElementById("errordoctor");
                    element.classList.remove("gu-hide");
                    return false;
                }else{
                    var element = document.getElementById("doctor");
                    element.classList.remove("radbox");
                    var element = document.getElementById("errordoctor");
                    element.classList.add("gu-hide");

                }
                // const checked = document.querySelector('#accept:checked') !== null;
                if (!(document.querySelector('#styled-checkbox-1:checked') !== null)) {
                    var element = document.getElementById("styled-checkbox-1");
                    element.classList.add("radbox");
                    var element = document.getElementById("errorstyled-checkbox-1");
                    element.classList.remove("gu-hide");
                    return false;
                    }else{
                    var element = document.getElementById("styled-checkbox-1");
                    element.classList.remove("radbox");
                    var element = document.getElementById("errorstyled-checkbox-1");
                    element.classList.add("gu-hide");
                    }

            }
      </script>
   </body>
</html>
