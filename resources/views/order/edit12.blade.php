    <div class="card bg-none card-box">
        <form class="pl-3 pr-3" method="post" onsubmit="return validateForm()" action="{{ route('order.update') }}">
            @csrf
            <input type="hidden" class="form-control" id="id" value="" name="id" />
            <div class="row">
            <div class="col-6 form-group">
                    <label class="form-control-label" for="name">{{ __('Full name') }}</label>
                    <input type="text" class="form-control" id="name" value="{{isset($order['name'])?$order['name']:''}}" name="name" required/>
                    <span class="gu-hide" style="color: red;" id="errorname">Name is required</span>
                    @if ($errors->has('name'))
                        <span class="invalid feedback"role="alert">
                            <div style="color: red;">{{ $errors->first('name') }}.</div>
                        </span>
                    @endif
                    <label class="form-control-label" for="mobile">{{ __('Mobile phone') }}</label>
                    <input type="text" class="form-control" id="mobile" value="{{isset($order['customer_phone_number'])?$order['customer_phone_number']:''}}" name="mobile" required/>
                    <span class="gu-hide" style="color: red;" id="errormobile">Mobile number is required</span>
                    @if ($errors->has('mobile'))
                        <span class="invalid feedback"role="alert">
                            <div style="color: red;">{{ $errors->first('mobile') }}.</div>
                        </span>
                    @endif

                    <label class="form-control-label" for="tablets">{{ __('Number of tablets') }}</label>
                    <input type="text" class="form-control" id="tablets" value="{{isset($order['number_of_tablet'])?$order['number_of_tablet']:''}}" name="tablets" required/>
                    <span class="gu-hide" style="color: red;" id="errortablets">Number of tablets is required</span>

                    <label class="form-control-label" for="doctor">{{ __('Doctor name') }}</label>
                    <input type="text" class="form-control" id="doctor" value="{{isset($order['doctor_name'])?$order['doctor_name']:''}}" name="doctor" required/>
                    <span class="gu-hide" style="color: red;" id="errordoctor">Doctor name is required</span>

                    <label class="form-control-label" for="hospital_name">{{ __('COMMISSION RATE (RP)') }}</label>
                    <input type="text" class="form-control" id="hospital_name" value="{{isset($order['commission_rate'])?$order['commission_rate']:''}}" name="hospital_name" required/>


                    <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
                    <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
                </div>
            </div>
        </form>
    </div>


  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
     integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
     crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
     integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
     crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
     integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
     crossorigin="anonymous"></script>
     <script>
    //from validation

        function validateForm() {


            let x = document.forms["myForm"]["name"].value;
            console.log(x);
            if (x == "") {
                var element = document.getElementById("name");
                element.classList.add("radbox");
                var element = document.getElementById("errorname");
                element.classList.remove("gu-hide");

                return false;
            }
            if (!/^[a-zA-Z' ']*$/g.test(x)) {
                var element = document.getElementById("name");
                element.classList.add("radbox");
                var element = document.getElementById("errorname");
                element.classList.remove("gu-hide");
                document.myForm.name.focus();
                return false;
            }else{
                var element = document.getElementById("name");
                element.classList.remove("radbox");
                var element = document.getElementById("errorname");
                element.classList.add("gu-hide");

            }
            let y = document.forms["myForm"]["mobile"].value;
            if (y == "") {
                var element = document.getElementById("mobile");
                element.classList.add("radbox");
                var element = document.getElementById("errormobile");
                element.classList.remove("gu-hide");
                //alert("Name must be filled out");
                return false;
            }else{
                var element = document.getElementById("mobile");
                element.classList.remove("radbox");
                var element = document.getElementById("errormobile");
                element.classList.add("gu-hide");

            }
            let j = document.forms["myForm"]["tablets"].value;
            if (j == "") {

                var element = document.getElementById("tablets");
                element.classList.add("radbox");
                var element = document.getElementById("errortablets");
                element.classList.remove("gu-hide");
                return false;
            }else{
                var element = document.getElementById("tablets");
                element.classList.remove("radbox");
                var element = document.getElementById("errortablets");
                element.classList.add("gu-hide");

            }
            let k = document.forms["myForm"]["doctor"].value;
            if (k == "") {

                var element = document.getElementById("doctor");
                element.classList.add("radbox");
                var element = document.getElementById("errordoctor");
                element.classList.remove("gu-hide");
                return false;
            }else{
                var element = document.getElementById("doctor");
                element.classList.remove("radbox");
                var element = document.getElementById("errordoctor");
                element.classList.add("gu-hide");

            }
            // const checked = document.querySelector('#accept:checked') !== null;
            if (!(document.querySelector('#styled-checkbox-1:checked') !== null)) {
                var element = document.getElementById("styled-checkbox-1");
                element.classList.add("radbox");
                var element = document.getElementById("errorstyled-checkbox-1");
                element.classList.remove("gu-hide");
                return false;
                }else{
                var element = document.getElementById("styled-checkbox-1");
                element.classList.remove("radbox");
                var element = document.getElementById("errorstyled-checkbox-1");
                element.classList.add("gu-hide");
                }

        }
  </script>
</body>
</html>
