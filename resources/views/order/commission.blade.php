

@extends('layouts.admin')

@if($type=="pharmacy")
        @section('title')
            {{ __('commission.Pharmacy Commission') }}
        @endsection
@endif
@if($type=="doctor")
        @section('title')
            {{ __('commission.Doctor Commission') }}
        @endsection
@endif
@if($type=="agent")
        @section('title')
            {{ __('commission.Agent Commission') }}
        @endsection
@endif
@section('action-button')
<!-- <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-body"> -->



 {{-- <input type="date" id="previousDate" value="{{isset($previousDate)?$previousDate:''}}" name="previousDate"> - <input type="date" id="nextDate" name="nextDate" value="{{isset($nextDate)?$nextDate:''}}">
<input type="hidden" value="{{$referral_code}}" id="referral_code" name="referral_code">
<button type="submit" id="submit" name="submit" >Search</button>  --}}
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <h5>Date Range</h5>
                        <div class="row">
                        <input type="hidden" value="{{$referral_code}}" id="referral_code" name="referral_code">
                        <div class="col-2 form-group">
                            {{ Form::label('issue_date', __('FROM'),['class'=>'form-control-label']) }}
                            {{ Form::text('previousDate',null, array('class' => 'form-control datepicker','id'=>'previousDate','required'=>'required')) }}
                        </div>
                        <div class="col-2 form-group">
                            {{ Form::label('due_date', __('To'),['class'=>'form-control-label']) }}
                            {{ Form::text('nextDate',null, array('class' => 'form-control datepicker','id'=>'nextDate','required'=>'required')) }}
                        </div>

                        <div class="col-8 form-group">
                            <div style="margin-top: 32px;text-align: right;">
                            <button type="submit" id="submit" name="submit" style="background-color: #0f5ef7; color: #fff; padding: 4px 20px; border: 0;border-radius: 5px;">Search</button>
                            </div>
                        </div>
                        </div>
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                                <th>{{__('commission.Order Id')}}</th>
                                <th>{{__('commission.customer Name')}}</th>
                                <th>{{__('commission.Order Date')}}</th>
                                <th>{{__('commission.Number Of Tablet')}}</th>
                                <th>{{__('commission.Commission Rate')}}</th>
                                <th>{{__('commission.Commission')}}</th>


                            </thead>
                            <tbody id='orders'>
                                @php $total=0;@endphp
                                    @if(isset($orders))
                                    @foreach ($orders as $order)
                                        <tr>
                                            <td>#{{isset($order['order_id'])?$order['order_id']:''}}</td>
                                            <td>{{isset($order['name'])?$order['name']:''}}</td>
                                            <td>{{isset($order['order_date'])?$order['order_date']:''}}</td>
                                            <td>{{isset($order['number_of_tablet'])?$order['number_of_tablet']:''}}</td>
                                            <td>{{isset($order['commission_rate'])?$order['commission_rate']:''}}</td>
                                            <td>{{isset($order['commission'])?$order['commission']:''}}</td>

                                        </tr>
                                        @php $total=$total+$order['commission']; @endphp
                                    @endforeach
                                    @endif
                                </tbody>
                        </table>
                        <div> Total Commission = <span id="total">{{$total}}</span> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

<script>

   </script>
