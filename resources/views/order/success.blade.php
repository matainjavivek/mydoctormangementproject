@include('order.layout.orderlayout')

@section('title')
    {{ __('Order') }}
@endsection
<body>
  <div class="bg-color">
    <div class="container">
      <div class="row">
        <div class="col-md-5 mx-auto">
          <div class="success-modal">
            <div class="success-modal-wrap">
              <div class="success-icon-wrap"><img src="{{ asset('assets/img/success-icon.png') }}" width="25%"></div>
              <div class="success-heading-text">Order Success</div>
              <div class="success-text">Your order has been confirmed.</div>
              @if (isset($referral_type) && isset($referral_code))
              <div><a href="{{ route('order.create',[$referral_type,$referral_code])}}" class="btn success-button" data-dismiss="modal">OK</a></div>
              @else
              <div><a href="{{ route('order')}}" class="btn success-button" data-dismiss="modal">OK</a></div>
              @endif

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
    integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
    integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
    crossorigin="anonymous"></script>
</body>

</html>
