
   @include('order.layout.orderlayout')

    @section('title')
        {{ __('Order Create') }}
    @endsection
   <body>
      <div class="bg-color">
         <div class="container">
            <div class="row">
               <div class="col-md-4 mx-auto">
                  <div class="text-center pt-4"><img src="{{ asset('assets/images/logo.png') }}" width="45%"></div>
                  <div class="contact">
                     <div class="text-center page-heading">Order Medicine</div>
                     <div class="form-area">
                        <form method="POST" onsubmit="return validateForm()" action="{{ route('order.store') }}" name="myForm" class="date-form" id="contact_form">
                            @csrf

                           <div class="group form-group">
                              <input type="text" class="form-control" id="name" name="name">
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Full name</label>
                              <span class="gu-hide" style="color: red;" id="errorname">Name is required</span>
                           </div>


                            @if ($errors->has('name'))
                                <span class="invalid feedback"role="alert">
                                    <div style="color: red;">{{ $errors->first('name') }}.</div>
                                </span>
                            @endif
                           <div class="group form-group">
                              <input type="tel" maxlength="10" minlength="10" class="form-control" name="mobile" id="mobile" >
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Mobile phone</label>
                                <span class="gu-hide" style="color: red;" id="errormobile">Mobile number is required</span>
                            </div>
                            @if ($errors->has('mobile'))
                                <span class="invalid feedback"role="alert">
                                    <div style="color: red;">{{ $errors->first('mobile') }}.</div>
                                </span>
                            @endif
                           <div class="group form-group">
                              <input type="number" class="form-control" name="tablets" id="tablets" >
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Number of tablets</label>
                              <span class="gu-hide" style="color: red;" id="errortablets">Number of tablets is required</span>
                           </div>
                           <div class="group form-group">
                              <input type="text" class="form-control" id="doctor" name="doctor">
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Doctor name</label>
                              <span class="gu-hide" style="color: red;" id="errordoctor">Doctor name is required</span>
                           </div>
                           <div class="group form-group marginB">
                              <input type="text" class="form-control" name="referral_code" id="referral_code" value="{{isset($referral_code)?$referral_code:''}}" >
                              <span class="highlight"></span>
                              <span class="bar"></span>
                              <label>Referral code (Optional)</label>
                              <span class="gu-hide" style="color: red;" id="errorreferral_code">Enter Valid Code</span>
                           </div>
                           <div id="referral_code_error_text" class="form-text error-text gu-hide">Please enter the correct
                                            referral code</div>
                           @if ($errors->has('referral_code'))
                                <span class="invalid feedback"role="alert">
                                    <div style="color: red;">{{ $errors->first('referral_code') }}.</div>
                                </span>
                            @endif
                           <div class="referrer-name">Referrer name :-   <span id="user-name">{{isset($user['name'])?$user['name']:''}}</span> </div>
                           <div class="small-text">
                                <div class="checkbox-wrap"><input class="styled-checkbox" id="styled-checkbox-1" name="checkbox" type="checkbox"
                                    value="value1" ></div>
                                    <span class="gu-hide" style="color: red;" id="errorstyled-checkbox-1">You need to check box</span>
                                     @if ($errors->has('checkbox'))
                                            <span class="invalid feedback"role="alert">
                                            <div style="color: red;">{{ $errors->first('checkbox') }}.</div>
                                        </span>
                                    @endif
                                <div class="checkbox-text-wrap">I agree to the <a
                                    href="#">Terms and conditions</a></div>
                            </div>
                            <input type="hidden" name="referral_type" value="{{isset($referral_type)?$referral_type:''}}">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary col-md-12">Submit</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
         integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
         crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
         integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
         crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
         integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
         crossorigin="anonymous"></script>
         <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
         <script src="{{asset('assets/js/user.js')}}"></script>
   </body>
</html>
