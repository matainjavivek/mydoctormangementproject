@extends('layouts.admin')





@section('title')
{{ __('messages.Manage Order') }}
@endsection

@section('action-button')

<div class="all-button-box row d-flex justify-content-end">

    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
        <a href="{{route('order.add')}}" class="btn btn-xs btn-white btn-icon-only width-auto" >
            <i class="fas fa-plus"></i> {{__('Add')}}
        </a>
    </div>

</div>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-body">
            
            <form method="GET"  action="{{ route('order.search') }}" name="myForm" id="contact_form">
                @csrf
                <h5>Date Range</h5>
            <div class="row">
              
                <div class="col-2 form-group">
                    {{ Form::label('issue_date', __('FROM'),['class'=>'form-control-label']) }}
                    {{ Form::text('previousDate',null, array('class' => 'form-control datepicker','id'=>'previousDate','required'=>'required')) }}
                </div>
                <div class="col-2 form-group">
                    {{ Form::label('due_date', __('To'),['class'=>'form-control-label']) }}
                    {{ Form::text('nextDate',null, array('class' => 'form-control datepicker','id'=>'nextDate','required'=>'required')) }}
                </div>

                <div class="col-8 form-group">
                    <div style="margin-top: 32px;text-align: right;">
                        <button type="submit" id="submit" name="submit" style="background-color: #0f5ef7; color: #fff; padding: 4px 20px; border: 0;border-radius: 5px;">Search</button>
                    </div>
                </div>
            </form>
            </div>
               
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('messages.Order id')}}</th>
                                <th>{{__('messages.Date')}}</th>
                                <th>{{__('messages.Name')}}</th>
                                <th>{{__('messages.Doctor name')}}</th>
                                <th>{{__('messages.Referral name')}}</th>
                                <th>{{__('messages.Phone number')}}</th>
                                <th>{{__('messages.Tablets')}}</th>
                                <th>{{__('messages.C.R (RP)')}}</th>
                                <th>{{__('messages.Commission (RP)')}}</th>
                                <th>{{__('messages.Status')}}</th>
                                <th width="200px">{{__('messages.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($orders))
                            @foreach ($orders as $key=>$order)
                                @php
                                    $image ='Name : '.$order['name'];

                                @endphp
                                <tr>
                                    <td>{{isset($order['order_id'])?'#'.$order['order_id']:''}}</td>
                                    <td>{{isset($order['order_date'])?$order['order_date']:''}}</td>
                                    <td>{{isset($order['name'])?$order['name']:''}}</td>
                                    <td>{{isset($order['doctor_name'])?$order['doctor_name']:''}}</td>
                                    <td><p>{{isset($order->user->name)?$order->user->name:''}}</p>
                                        <span class= "urlbtn  copy-button">{{isset($order->user->type)?$order->user->type:''}}</span>
                                    </td>
                                    <td>{{isset($order['customer_phone_number'])?$order['customer_phone_number']:''}}</td>
                                    <td>{{isset($order['number_of_tablet'])?$order['number_of_tablet']:''}}</td>
                                    <td>{{isset($order['commission_rate'])?$order['commission_rate']:''}}</td>
                                    <td>{{isset($order['commission'])?$order['commission']:''}}</td>
                                    <td>{{isset($order['status'])?$order['status']:''}}</td>
                                    <td class="Action">
                                        <span>
                                            <a class="edit-icon mx-2" href="{{ route('order.edit',[$order['order_id']]) }}"><i class="fas fa-pencil-alt" title="Edit"></i></a>

                                            {{-- <a href="{{ route('order.status',[$order['order_id']]) }}" class="edit-icon mx-2"><i class="fas fa-eye-dropper"></i></a> --}}

                                            <a href="#" data-url="{{ route('order.status',[$order['order_id']]) }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Edit Status')}}" class="edit-icon"><i class="fas fa-eye-dropper" title="Status"></i></a>

                                            <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$order['order_id']}}').submit();" ><i class="fas fa-trash" title="Delete"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['order.destroy', $order['order_id']],'id'=>'delete-form-'.$order['order_id']]) !!}
                                            {!! Form::close() !!}


                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

