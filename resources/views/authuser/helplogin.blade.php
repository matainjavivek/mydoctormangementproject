@include('partials.user.header')
</head>

<body>


    <div class="help">
        <div class="top-header mob_display-none">
            <div class="container">
                <div class="row">
                    <div class="col"><a href="#"><img src="{{ asset('frontend/images/logo.png')}}" class="logoW"
                                alt="logo"></a></div>
                    <div class="col text-end"><img src="{{ asset('frontend/images/user-icon.png')}}" alt="user" class="userW"></div>
                </div>
            </div>
        </div>
        <div class="bg-color">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8 mx-auto contenerP0">
                        <div class="login-icon"><img src="{{ asset('frontend/images/login.png')}}" width="8%"></div>
                        <div class="help-contact-wrap">
                            <div class="help-contact">
                                <div class="help-contact-heading">Need help loggin in?</div>
                                <div class="content-box">
                                    <div>Problem signing in ?</div>
                                    <div class="customer-service-button-wrap">
                                        <a href="{{ route('authuser.customer') }}">
                                        <button class="customer-service-button">
                                            Customer Service
                                            <div class="whatsapp-image-wrap"><img src="{{ asset('frontend/images/whatsapp-icon.png')}}" alt="whatsapp" width="18%"></div>
                                        </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer mob_display-none">
            <div class="footer-wrap">
                <div class="my-auto tentang"><a href="#">TENTANG KAMI</a></div>
                <a href="#">
                    <div class="whatsapp_btn py-2 px-3">
                        <i class="fa-brands fa-whatsapp"></i>
                        <span class="ps-2">Customer Service</span>
                    </div>
                </a>
            </div>
        </div>
    </div>









</body>

</html>
