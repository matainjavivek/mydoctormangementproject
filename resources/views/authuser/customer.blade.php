
@extends('partials.user.header')


@section('front_title')
    {{ __('Sleep Center') }}
@endsection
@section('content')

    <div class="customer_srv">


        <div class="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="ask_help m-3">
                            <div class="text_1">IT’S OKAY TO</div>
                            <div class="text_2 mb-4">ASK FOR HELP</div>
                            <div class="text_3 mb-3">CARI DOKTER SPKJ DI KOTA ANDA</div>
                            <div class="input_text">

                                <form action="{{ route('front.closestCarriers') }}" method="GET">
                                <button class="search_btn gu-hide" id="locationsearch" type="submit">
                                    search
                                </button>

                                <input type="text" id="address" name="address"  class="form-control border-0 rounded-3" placeholder="Type City"></div>

                                <input type="hidden" id="lat" name="latitude" value="{{isset($latitude)?$latitude:''}}">
                                <input type="hidden" id="lon" name="longitude" value="{{isset($longitude)?$longitude:''}}">
                                <input type="hidden" id="city" name="city" value="{{isset($city)?$city:''}}">

                                <div id="map" style="width: 100%; height: 0px;top:4px"></div>

                                </form>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                    <div class="kirem_resep mx-3 mt-3">
                        <div class="kirem_resep_top p-4">
                            <div class="row m-0">
                                <div class="col-9 p-0">
                                    <div class="text_1 mb-2">PUNYA RESEP OBAT ?</div>
                                    <div class="text_2">Pastikan anda mendapatkan</div>
                                    <div class="text_3 mb-4">obat asli dari apotik terpercaya</div>
                                </div>
                                <div class="col-3 p-0">
                                    <div class="picture"><img src="{{ asset('frontend/images/medicine-img.jpg')}}" alt=""></div>
                                </div>
                            </div>
                        </div>
                        <div class="kirem_resep_bottom">
                            <a href="#"><div class="kirim-btn text-center mx-auto mt-4 py-3">Kirim Recep</div></a>
                        </div>
                    </div>
                    </div>
                </div>
                </div>


                <div class="slider mt-5">
                    <div class="slider_title ps-3 mb-4">In the spotlight</div>
                    <div class="slider_wrap">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="slider_cell slider_cell-01">
                                    <div class="icon_text">QUIZ</div>
                                    <div class="slider_para">
                                        Apakah Anda Mengalami INSOMNIA ?
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slider_cell slider_cell-02">
                                    <div class="slider_para ms-auto">
                                        Apakah Anda Mengalami INSOMNIA ?
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slider_cell slider_cell-03">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slider_cell slider_cell-04">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
                </div>

                <div class="container">
                <div class="row mt-5">
                    <div class="col-md-6 col-sm-6 col-12">
                    <div class="panic p-3 mx-3">
                        <div class="row m-0">
                            <div class="col-4">
                                <div class="pic"><img src="{{ asset('frontend/images/panick_img.png')}}" alt=""></div>
                            </div>
                            <div class="col-8">
                                <div class="text_1 my-2">PANIC ATTACK ?</div>
                                <div class="text_2">Kenali Gejalanya</div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="panic_hotline text-center p-2 mx-3">
                            <div class="text_1">HOTLINE KEMENKES</div>
                            <div class="text_2 mb-3">PENCEGAHAN BUNUH DIRI</div>
                            <div class="text_3 mb-2 mx-auto">021-500-454</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="gap"></div>

    </div>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAwQ3Gm7MIQ61LSG4u2s6XBuZyLZMj47CE&callback=initMap" async defer></script>
    <script src="{{ asset('frontend/js/gmap.js') }}"></script>
</body>
</html>
@endsection
