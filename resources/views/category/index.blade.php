@extends('layouts.admin')

@section('title')
    {{ __('messages.Manage Category') }}
@endsection

@section('action-button')
    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('messages.Create Category')}}" data-url="{{route('category.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div>


    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('Id')}}</th>
                                <th>{{__('messages.Name')}}</th>
                                <th>{{__('messages.Description')}}</th>
                                 <th>{{__('Image')}}</th>
                                <th width="200px">{{__('messages.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($categories))
                            @foreach ($categories as $key=>$category)
                                <tr>
                                <td class="Permission">{{isset($category['id'])?$category['id']:''}}</td>
                                    <td class="Permission">{{isset($category['name'])?$category['name']:''}}</td>
                                    <td class="Permission">{{isset($category['description'])?$category['description']:''}}</td>
                                   
                                    <td class="Permission"> <img alt="image" class="mr-3 rounded-circle" width="50" height="50" src="@if($category['image']){{asset('/storage/category/'.$category['image'])}} @else {{asset('assets/img/avatar/avatar-1.png')}} @endif"></td>

                                    <td class="Action">
                                        <span>

                                            <a href="#" data-url="{{ URL::to('category/'.$category['id'].'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('messages.Edit category')}}" class="edit-icon"><i class="fas fa-pencil-alt" title="Edit"></i></a>

                                            <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$category['id']}}').submit();"><i class="fas fa-trash" title="Delete"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['category.destroy', $category['id']],'id'=>'delete-form-'.$category['id']]) !!}
                                            {!! Form::close() !!}



                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
