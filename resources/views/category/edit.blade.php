<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" >
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('category.update') }}" onsubmit="return validCategory()" name="myForm"  enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$categories->id}}">
        <div class="row">
            <div class="col-12 form-group">
                
                <!-- <div>
                <label class="form-control-label" for="id">{{__('messages.id')}}</label>
                <input type="number" class="form-control" id="id" id="id" value="{{$categories->id}}"/>
                <span class="gu-hide" style="color: red;" id="errorid">{{__('validation.id_required')}}</span>

                </div>  -->
                  <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                        <div class="choose-file">
                            <label for="image">
                                <div>{{__('Choose file here')}}</div>
                                <input class="form-control" name="image" type="file" id="image" accept="image/*" data-filename="profile_update">
                            </label>
                            <p class="profile_update"></p>
                        </div>
                        @error('avatar')
                        <span class="invalid-feedback text-danger text-xs" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <span class="clearfix"></span>
                    <span class="text-xs text-muted">{{ __('Please upload a valid image file. Size of image should not be more than 2MB.')}}</span>
                </div>

                
                <div>
                <label class="form-control-label" for="name">{{__('messages.Name')}}</label>
                <input type="text" class="form-control" id="name" name="name" value="{{$categories->name}}" />
                <span class="gu-hide" style="color: red;" id="errorname">{{__('validation.Name_required')}}</span>
                </div>

                <div>
                <label class="form-control-label" for="description">{{__('messages.Description')}}</label>
                <input type="text" class="form-control" id="description" name="description" value="{{$categories->description}}" />
                <span class="gu-hide" style="color: red;" id="errordescription">{{__('validation.description_required')}}</span>
                </div>
                <input type="submit" value="{{__('Update')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>

        </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>
</div>
