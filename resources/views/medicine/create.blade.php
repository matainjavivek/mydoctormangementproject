<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('medicine.store') }}" onsubmit="return validAuthMedicine()" name="myForm">
        @csrf
        <div class="row">
            <div class="col-12 form-group">
                <div>
                <label class="form-control-label" for="name">{{__('messages.Name')}}</label>
                <input type="text" class="form-control" id="name" name="name" />
                <span class="gu-hide" style="color: red;" id="errorname">{{__('validation.Name_required')}}</span>
                </div>
                <div>
                <label class="form-control-label" for="description">{{__('messages.Description')}}</label>
                <input type="text" class="form-control" id="description" name="description" />
                <span class="gu-hide" style="color: red;" id="errordescription">{{__('validation.Description_required')}}</span>
                </div>
                <div>
                <label class="form-control-label" for="price">{{__('messages.price')}}</label>
                <input type="number" class="form-control" id="price" name="price" />
                <span class="gu-hide" style="color: red;" id="errorprice">{{__('validation.Price_required')}}</span>
                </div>
       
                <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>

</div>
