@extends('layouts.admin')

@section('title')
    {{ __('messages.Manage Medicine') }}
@endsection

@section('action-button')
    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('messages.Add medicine')}}" data-url="{{route('medicine.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('messages.Name')}}</th>
                                <th>{{__('messages.Description')}}</th>
                                <th>{{__('messages.price')}}</th>
                                <th width="200px">{{__('messages.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($medicines))
                            @foreach ($medicines as $key=>$medicine)

                                <tr>
                                    <td class="Permission">{{isset($medicine['name'])?$medicine['name']:''}}</td>
                                    <td class="Permission">{{isset($medicine['description'])?$medicine['description']:''}}</td>
                                    <td class="Permission">{{isset($medicine['price'])?$medicine['price']:''}}</td>



                                    <td class="Action">
                                        <span>

                                            <a href="#" data-url="{{ URL::to('medicine/'.$medicine['id'].'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Edit medicine')}}" class="edit-icon"><i class="fas fa-pencil-alt" title="Edit"></i></a>

                                            <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$medicine['id']}}').submit();"><i class="fas fa-trash" title="Delete"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['medicine.destroy', $medicine['id']],'id'=>'delete-form-'.$medicine['id']]) !!}
                                            {!! Form::close() !!}

                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>

                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
