<footer class="main-footer bottom-0 px-4 py-4">
    <div class="footer-left">
         {{ (Utility::getValByName('footer_text')) ? Utility::getValByName('footer_text') :config('app.name', 'Federal Ministry Of Health') }} {{date('Y')}}
    </div>
    <div class="footer-right">
    </div>
</footer>
