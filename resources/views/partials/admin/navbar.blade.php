<div class="sidenav custom-sidenav" id="sidenav-main">
    <div class="sidenav-header d-flex align-items-center">
        <a class="navbar-brand" href="{{route('home')}}">
            <img src="{{ asset('logo/doctor_plus.svg') }}" alt="doctor plus" class="navbar-brand-img">
        </a>
        <div class="ml-auto">
            <div class="sidenav-toggler sidenav-toggler-dark d-md-none" data-action="sidenav-unpin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line bg-white"></i>
                    <i class="sidenav-toggler-line bg-white"></i>
                    <i class="sidenav-toggler-line bg-white"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="scrollbar-inner">
        <div class="div-mega">
            <ul class="navbar-nav navbar-nav-docs">
                @if((\Auth::user()->type != 'Owner'))
                <li class="nav-item" style="font-weight: 400;">
                    {{ Auth::user()->org_name }}
                </li>
                @endif
                <li class="nav-item">
                    <a href="{{route('home')}}" class="nav-link {{ (Request::route()->getName() == 'home') ? 'active' : '' }}">
                        <i class="fas fa-fire"></i>{{__('messages.Dashboard')}}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->is('zone*') ? 'active' : '' }}" href="{{route('zone.index')}}">
                    <i class="fas fa-map-marker-alt"></i>{{__('Zone')}}

                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('advertise*') ? 'active' : '' }}" href="{{route('advertise.index')}}">
                    <i class="fas fa-map-marker-alt"></i>{{__('Advertise')}}

                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link {{ request()->is('spot*') ? 'active' : '' }}" href="{{route('spot.index')}}">
                    <i class="fas fa-map-marker-alt"></i>{{__('Spot ')}}

                    </a>
                </li> -->
                @can('Manage Pharmacys')
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('pharmacy*') ? 'active' : '' }}" href="{{route('pharmacy')}}">
                    <i class="fa-solid fa-house-chimney-medical"></i>{{__('messages.Pharmacy')}}
                    </a>
                </li>
                @endcan
                @can('Manage Agents')
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('agent*') ? 'active' : '' }}" href="{{route('agent')}}">
                    <i class="fa-solid fa-user-plus"></i>{{__('messages.Agent')}}
                    </a>
                </li>
                @endcan

                @can('Manage Doctors')
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('doctor*') ? 'active' : '' }}" href="{{route('doctor')}}">
                    <!-- <img src="{{ asset('assets/img/doctor-icon-male_1.svg') }}" alt="Sleep center" class="navbar-brand-img">   -->
                    <i class="fa-solid fa-user-doctor"></i>{{__('messages.Doctor')}}
                    </a>
                </li>
                @endcan
                

                @can('Manage Clinics')
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('clinic*') ? 'active' : '' }}" href="{{route('clinic.index')}}">
                     <i class='fas fa-building'></i>{{__('messages.clinic')}}

                    </a>
                </li>
                @endcan

                @can('Manage Clinics')
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('category*') ? 'active' : '' }}" href="{{route('category.index')}}">
                    <i class="fa fa-stethoscope"></i>{{__('Category')}}

                    </a>
                </li>
                @endcan

                {{-- @can('Manage Medicines')
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('medicine*') ? 'active' : '' }}" href="{{route('medicine.index')}}">
                    <i class="fa fa-medkit" aria-hidden="true"></i>{{__('messages.Medicine')}} </a>
                 </li>
                 @endcan --}}
                 
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('degree*') ? 'active' : '' }}" href="{{route('degree.index')}}">
                    <i class="fa fa-stethoscope"></i>{{__('Degree')}}

                    </a>
                </li>
               
                 @can('Manage Orders')
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('order*') ? 'active' : '' }}" href="{{route('order')}}">
                    <!-- <img src="{{ asset('assets/img/doctor-icon-male_1.svg') }}" alt="Sleep center" class="navbar-brand-img">   -->
                    <i class="fa-brands fa-first-order-alt"></i>{{__('messages.Order')}}
                    </a>
                </li>
                @endcan
                @if(Gate::check('Manage Users') || Gate::check('Manage Clients') || Gate::check('Manage Roles') || Gate::check('Manage Permissions'))
                    @if((\Auth::user()->type == 'Owner') || (\Auth::user()->type == 'Admin'))
                        @can('Manage Users')
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('users*') ? 'active' : '' }}" href="{{route('users')}}">
                                    <i class="fas fa-users"></i>{{__('messages.Users')}}
                                </a>
                            </li>
                        @endcan

                    @endif
                    @if((\Auth::user()->type == 'Owner'))
                        @can('Manage Roles')
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::route()->getName() == 'roles.index') ? 'active' : '' }}" href="{{route('roles.index')}}">
                                    <i class="fas fa-user-cog"></i>{{__('messages.Roles')}}
                                </a>
                            </li>
                        @endcan
                    @endif
                    @if((\Auth::user()->type == 'Owner') || (\Auth::user()->type == 'Admin'))
                        @can('System Settings')
                        <li class="nav-item">
                            <a class="nav-link {{ (Request::route()->getName() == 'settings') ? 'active' : '' }}" href="{{route('settings')}}">
                                <i class="fas fa-cogs"></i>{{__('messages.System Settings')}}
                            </a>
                        </li>
                        @endcan
                    @endif
                @endif
                    @can('Manage Reports')
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('report*') ? 'active' : 'collapsed' }}" href="#navbar-getting-started" data-toggle="collapse" role="button" aria-expanded="{{ request()->is('report*')? 'true' : 'false' }}" aria-controls="navbar-getting-started">
                                <i class="fas fa-cog"></i>{{__('messages.Report')}}
                                <i class="fas fa-sort-up"></i>
                            </a>
                            <div class="collapse {{ request()->is('report*') ? 'show' : '' }}" id="navbar-getting-started">
                                <ul class="nav flex-column submenu-ul">

                                    <li class="nav-item {{ request()->is('report/search/daily') ? 'active' : '' }}">
                                        <a class="nav-link" href="{{route('report',['daily'])}}">{{__('messages.Daily')}}</a>
                                    </li>

                                    <li class="nav-item {{ request()->is('report/search/monthly') ? 'active' : '' }}">
                                        <a class="nav-link" href="{{route('report',['monthly'])}}">{{__('messages.Monthly')}}</a>
                                    </li>

                                    <li class="nav-item {{ request()->is('report/search/date') ? 'active' : '' }}">
                                        <a class="nav-link" href="{{route('report',['date'])}}">{{__('messages.Search By Date')}}</a>
                                    </li>

                                </ul>
                            </div>
                        </li>
                    @endcan
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i>
                            <span>{{__('auth.Logout')}}</span>
                        </a>
                </li>
            </ul>
        </div>
    </div>
</div>
