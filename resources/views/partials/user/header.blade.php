<!DOCTYPE html>
<html lang="en">
<head>
<title> @yield('front_title')</title>
  <!-- <title>sleep_center-index</title> -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>


  <link rel="stylesheet" href="{{ asset('frontend/css/slider.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/customer_slider.css') }}">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">


  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
</head>
<body>
        @yield('headerNone')
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col"><a href="{{ route('authuser.customer') }}"><img src="{{ asset('frontend/images/logo.png')}}" class="logoW" alt="logo"></a></div>
                    <a href="{{ route('authuser.profile') }}" class="col text-end"><img src="{{ asset('frontend/images/user-icon.png')}}" alt="user" class="userW"></a>
                </div>
            </div>
        </div>
        @yield('headerNoneEnd')
        <!-- <div class="customer_srv">
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col"><a href="{{ route('authuser.customer') }}"><img src="{{ asset('frontend/images/logo.png')}}" class="logoW" alt="logo"></a></div>
                    <a href="{{ route('authuser.profile') }}" class="col text-end"><img src="{{ asset('frontend/images/user-icon.png')}}" alt="user" class="userW"></a>
                </div>
            </div>
        </div> -->
  @yield('content')
  @extends('partials.user.footer')

  <!-- daftar -->









