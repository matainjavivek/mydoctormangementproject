<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('doctor.store') }}"  onsubmit="return validAuthDoctor()" name="myForm" enctype="multipart/form-data">
        
        @csrf
        <div class="row">
            <div class="col-12 form-group">
                {{-- <div class="form-group">
                    <input type="file" id="image" name="image" class="@error('image') is-invalid @enderror form-control">
                    <label for="exampleInputEmail1">Please Select Image</label>
                    
                    @error('image')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                </div> --}}

                <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                        <div class="choose-file">
                            <label for="image">
                                <div>{{__('Choose file here')}}</div>
                                <input class="form-control" name="image" type="file" id="image" accept="image/*" data-filename="profile_update">
                            </label>
                            <p class="profile_update"></p>
                        </div>
                        @error('avatar')
                        <span class="invalid-feedback text-danger text-xs" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <span class="clearfix"></span>
                    <span class="text-xs text-muted">{{ __('Please upload a valid image file. Size of image should not be more than 2MB.')}}</span>
                </div>

                <div>
                    <label class="form-control-label" for="name">{{__('messages.Name')}}</label>
                    <input type="text" class="form-control" id="name" name="name" />
                    <span class="gu-hide" style="color: red;" id="errorname">{{__('validation.Name_required')}}</span>
                </div>
                <!-- <label class="form-control-label" for="last_name">{{ __('LAST NAME') }}</label>
                <input type="text" class="form-control" id="last_name" name="last_name" /> -->
                <!-- <div>
                    <label class="form-control-label" for="city">{{__('messages.City')}}</label>
                    <input type="text" class="form-control" id="city" name="city" />
                    <span class="gu-hide" style="color: red;" id="errorcity">{{__('validation.City_required')}}</span>
                </div>
                <div>
                    <label class="form-control-label" for="province">{{__('messages.Province')}}</label>
                    <input type="text" class="form-control" id="province" name="province" />
                    <span class="gu-hide" style="color: red;" id="errorprovince">{{__('validation.Province_required')}}</span>
                </div> -->

                <div>
                <label class="form-control-label" for="category">{{__('Specialist')}}</label>
                            <select id="category" name="category" class="form-control select2">
                                <option value="">--- Select category ---</option>
                                @foreach ($categories as $key => $category)

                                    <option value="{{$category->id}}">{{ $category->name }}</option>
                                @endforeach
                                
                            </select> 
                    <span class="gu-hide" style="color: red;" id="errorcategory">{{__('Category_required')}}</span>
                </div>

                <div>
                    <label class="form-control-label" for="hospital_name">{{__('messages.Hopital Name')}}</label>
                    <input type="text" class="form-control" id="hospital_name" name="hospital_name" />
                    <span class="gu-hide" style="color: red;" id="errorhospital_name">{{__('validation.Hospital_name_required')}}</span>
                </div>
                <div>
                    <label class="form-control-label" for="referral_code">{{__('Registration Code')}}</label>
                    <input type="text" class="form-control" id="referral_code"  name="referral_code" />
                    <span class="gu-hide" style="color: red;" id="errorreferral_code">{{__('validation.referral_code_required')}}</span>
                </div>
                <label class="form-control-label" for="experience">{{__('Experience')}}</label>
                <div class="col-12">
                    <input type="text" class="form-group col-4"  name="experience_year" /><span class="form-group col-2">Year</span> 
                    
                    <input type="text" class="form-group col-4"  name="experience_months" /> <span class="form-group col-2">Months</span>
                        
                        <!-- <span class="gu-hide" style="color: red;" id="errorexperience">{{__('validation.Hospital name_required')}}</span> -->
                   
                      
                </div>
                <div>
                    <label class="form-control-label" for="address">{{__('messages.Address')}}</label>
                    <input id="address" type="text" class="form-control controls"  name="address" />
                    <span class="gu-hide" style="color: red;" id="erroraddress">{{__('validation.address_required')}}</span>
                </div>

             
                
                <input type="submit" value="{{__('Create')}}"  class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
            
        </div>
    </form>
</div>
<script src="{{asset('assets/js/user.js')}}"></script>
