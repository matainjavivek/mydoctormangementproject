
    <div class="row">
        <div class="col-12">

            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade " id="business-setting" role="tabpanel" aria-labelledby="profile-tab3"> </div>
                <div class="tab-pane fade fade show active" id="system-setting" role="tabpanel" aria-labelledby="profile-tab3">
                    <form method="POST"  action="{{ route('doctor.update.category') }}" name="myForm" class="date-form" id="contact_form">
                        @csrf
                        <input type="hidden" name="doctor_id" value="{{isset($id)?$id:''}}">
                        <div class="card bg-none">
                            <div class="row company-setting">
                                <div class="col-10 form-group">
                                    {{-- <label class="form-control-label" for="role">{{__('category')}}</label> --}}
                                    <div class="col-12 form-group">
                                        {{ Form::label('category', __('category'),['class'=>'form-control-label']) }}
                                        {{ Form::select('category[]', $categories->pluck('name','id'),null, array('class' => 'form-control select2','multiple'=>'','required'=>'required')) }}
                                    </div>
                                </div>
                                <div class="form-group col-md-2 text-left" style="padding-top: 22px;">
                                    <input type="submit" id="save-btn" value="{{__('Save')}}" class="btn-create badge-blue">
                                </div>

                            </div>
                            <div class="card col-12">
                                <table class="table align-items-center mb-0">
                                    <tbody class="list">
                                            @if($doctorCategories)
                                                @foreach($doctorCategories as $key =>$doctorCategory)
                                                    <tr>
                                                    <td class="col-4">{{isset($doctorCategory->categoryname->name)?$doctorCategory->categoryname->name:""}}</td>

                                                    <td class="col-6">{{isset($doctorCategory->categoryname->description)?$doctorCategory->categoryname->description:""}}</td>
                                                    <td class="col-2">
                                                    <a href="{{route('doctor.destroy.category', [$doctorCategory['id']])}}" class="delete-icon mx-2"  ><i class="fas fa-trash" title="Delete"></i></a>
                                                    {{-- {!! Form::open(['method' => 'DELETE', 'route' => ['doctor.destroy', $doctorCategory['id']],'id'=>'delete-form-'.$doctorCategory['id']]) !!}
                                                    {!! Form::close() !!} --}}
                                                    </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

