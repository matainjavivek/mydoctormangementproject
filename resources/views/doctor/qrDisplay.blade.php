<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('pharmacy.update') }}">
        @csrf

        @php
            $image =QrCode::size(300)->generate($img);

        @endphp
        <div class="row">
            <div class="col-6 form-group">
                {{$image}}
                <p>{{$unique}}</p>
                <a href="/pharmacy/download">Download Now</a>
            </div>


            <div class="form-group col-12 text-right">
            <a href="/images/myw3schoolsimage.jpg" download>
                <input type="submit" value="{{__('Downlode')}}" class="btn-create badge-blue"></a>
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>
    </form>
</div>
