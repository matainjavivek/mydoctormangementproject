<div class="row">
        <div class="col-12">

            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade " id="business-setting" role="tabpanel" aria-labelledby="profile-tab3"> </div>
               
                <div class="tab-pane fade fade show active" id="system-setting" role="tabpanel" aria-labelledby="profile-tab3">
                  
                    <form method="POST"  action="{{route('doctor.education.add')}}" name="myForm" class="date-form" id="contact_form">
                        @csrf
                         <input type="hidden" name="doctor_id" value="{{isset($id)?$id:''}}">
                        <input type="hidden" name="total_doctor" id="total_doctor" value="0">
                        {{--  <input type="hidden" name="degree" >
                        <input type="hidden" name="institutename" > --}}
                        <div class="card bg-none">
                              
                              <div class="col-md-12 text-right" onclick="EducationDetails()"><i class="fas fa-plus"></i></div>
                            <div class="row company-setting" id="add_doctor">
                                {{-- <div class="col-12"> --}}
                                    
                                      <div class="col-3 form-group">
                                          {{ Form::label('degree', __('Degree'),['class'=>'form-control-label']) }}
                                          {{ Form::select('degree[]', $degrees->pluck('name','id'),null, array('class' => 'form-control select2','required'=>'required')) }}
                                      </div>
                                                  {{-- <div class="col-9 form-group">
                                          {{ Form::label('institutename', __('Institute name'),['class'=>'form-control-label']) }}
                                          {{ Form::select('institutename', $institutenames,null, array('class' => 'form-control select2','required'=>'required')) }}
                                      </div> --}}
                                    <div  class="col-8 form-group">
                                        <label class="form-control-label" for="institutename">{{__('Institute name')}}</label>
                                        <input type="text" class="form-control" id="institutename" name="start_institutenames_0" />
                                        <span class="gu-hide" style="color: red;" id="institutename">{{__('validation.institutename')}}</span>
                                    </div>
                                    
                                      

                                      
                            </div>
                            <script> var i=0; </script>

                            {{-- <div  class="col-10 form-group">
                                <label class="form-control-label" for="institutename">{{__('Institute name')}}</label>
                                <input type="text" class="form-control" id="institutename" name="institutename" />
                                <span class="gu-hide" style="color: red;" id="institutename">{{__('validation.institutename')}}</span>
                             </div> --}}

                              {{-- <div  class="col-10 form-group">
                                <label class="form-control-label" for="internship">{{__('Internship Institute')}}</label>
                                <input type="text" class="form-control" id="internship" name="internship" />
                                <span class="gu-hide" style="color: red;" id="errorinternship">{{__('validation.Internship_required')}}</span>
                             </div> --}}


                            <div class="form-group col-md-2 text-left" style="padding-top: 22px;">
                              <input type="submit" id="save-btn" value="{{__('Save')}}" class="btn-create badge-blue">
                          </div>
                            <div class="card col-12">
                                <table class="table align-items-center mb-0">
                                    <thead>
                                        <tr>
                                            
                                            <th>{{__('Degree')}}</th>
                                            <th>{{__('Institute name')}}</th>
                                            {{-- <th>{{__('Internship Institute')}}</th> --}}
                                            <th>{{__('messages.Action')}}</th>
                                        </tr>
                                        </thead>
                                    <tbody class="list">
                                      
                                            @foreach($doctor_details as $key =>$doctor_detail)
                                           
                                            
                                                <tr>
                                                

                                                <td class="col-6">{{isset($doctor_detail->degree)?$doctor_detail->degree:""}}</td>

                                                <td class="col-6">{{isset($doctor_detail->institute_name)?$doctor_detail->institute_name:""}}</td>
                                               
                                                {{-- <td class="col-6">{{isset($doctor_detail->internship_institute)?$doctor_detail->internship_institute:""}}</td> --}}

                                               
                                                <td class="col-6"> <a href="{{route('doctor.education.destroy',[$doctor_detail->id])}}" class="delete-icon mx-2"  ><i class="fas fa-trash" title="Delete"></i></a></td>
                                               
                                                
                                                </tr>
                                           
                                            @endforeach
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
       
    </div>

    <script src="{{asset('assets/js/education.js')}}"></script>