@extends('layouts.admin')

@section('title')
    {{ __('messages.Manage doctor') }}
@endsection

@section('action-button')
    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('messages.Create doctor')}}" data-url="{{route('doctor.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div>


    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('messages.Name')}}</th>
                                <th>{{__('messages.City')}}</th>
                                <th>{{__('messages.Province')}}</th>
                                <th>{{__('messages.Hospital Name')}}</th>
                                <th>{{__('messages.Address')}}</th>
                                <th>{{__('messages.Referral Code')}}</th>
                                <th>{{__('messages.Url')}}</th>
                                <th>{{__('messages.QR')}}</th>
                                <th width="200px">{{__('messages.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($doctors))
                            @foreach ($doctors as $key=>$doctor)
                                @php
                                    $image ='Name : '.$doctor['name'].', City : '.$doctor['city'].', Province : '.$doctor['province'].', Hospital Name : '.$doctor['hospital_name'].', referral_code : '.$doctor['referral_code'];
                                    if(isset($doctor['referral_code'])){
                                    $url="http://" . $_SERVER['HTTP_HOST']."/order/create/pharmacy/".$doctor['referral_code'];
                                    }

                                @endphp
                                <tr>
                                    <!-- <td class="Permission">{{isset($doctor['first_name'])?$doctor['first_name'].' '.$doctor['last_name']:''}}</td> -->
                                    <td class="Permission">{{isset($doctor['name'])?$doctor['name']:''}}</td>
                                    <td class="Permission">{{isset($doctor['city'])?$doctor['city']:''}}</td>
                                    <td class="Permission">{{isset($doctor['province'])?$doctor['province']:''}}</td>
                                    <td class="Permission">{{isset($doctor['hospital_name'])?$doctor['hospital_name']:''}}</td>
                                    <td class="Permission">{{isset($doctor['address'])?$doctor['address']:''}}</td>
                                    <td><p class= "code" id="copycode-{{$doctor['id']}}">{{isset($doctor['referral_code'])?$doctor['referral_code']:''}} </p><a class= "button copy-button"  href="#"  id="copy-{{$doctor['id']}}" onclick="CopyToClipboard('{{$doctor['id']}}');return false;">Copy</a></td>

                                    <td>
                                    <!-- <p class= "urlcode" id="copycode-{{$doctor['short_url']}}"><a href="{{route('url',[$doctor['short_url']])}}">
                                        {{isset($doctor['short_url'])?$doctor['short_url']:''}}
                                    </a></p> -->

                                    <!-- <div class= "urlbtn  copy-button"  href="#" data-ajax-popup="true" data-url="{{route('copy',['doctor',$doctor['referral_code']])}}"  id="copy-{{$doctor['short_url']}}" >Copy</div> -->
                                    <p  class="copy" type="hidden" id="copy{{$doctor['id']}}">{{$url}} </p><a class="copyUrl" href="#"  id="copyUrl-{{$doctor['id']}}" onclick="Copy({{$doctor['id']}}) ;return false;">copy</a>

                                    <span class="url" id="url"><a href="https://api.whatsapp.com/send?text={{ $url}}"><i class="td-share fas fa-share-alt" title="Share"></i></a></span>



                                    </td>




                                    <td >
                                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                                            <a href="#" class="QR" data-ajax-popup="true" data-title="" data-url="{{route('doctor.qrview',[$image,$doctor['referral_code']])}}">
                                                 {{__('VIEW')}}
                                            </a>
                                        </div>
                                    </td>

                                    <td class="Action">
                                        <span>

                                            <a href="#" data-url="{{ URL::to('doctor/'.$doctor['id'].'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('messages.Edit doctor')}}" class="edit-icon"><i class="fas fa-pencil-alt" title="Edit"></i></a>

                                            <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$doctor['id']}}').submit();"><i class="fas fa-trash" title="Delete"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['doctor.destroy', $doctor['id']],'id'=>'delete-form-'.$doctor['id']]) !!}
                                            {!! Form::close() !!}

                                            <a href="{{route('order.commission',[$doctor['referral_code']])}}" class="edit-icon"><i class="fa-solid fa-indian-rupee-sign" title="Commission"></i></a>

                                            <a href="#" data-url="{{route('doctor.chamber',[$doctor['id']])}}"  data-size="lg" data-ajax-popup="true" data-title="{{__('Edit chamber')}}" class="edit-icon mx-2"><i class="fa-solid fa-house-chimney-medical"></i></a>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
