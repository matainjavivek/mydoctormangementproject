@extends('layouts.admin')

@section('title')
    {{ __('Advertise') }}
@endsection

 @section('action-button')
    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('Create Advertise')}}" data-url="{{route('advertise.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div> 


    </div>
@endsection 

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('Id')}}</th>
                                <th>{{__('name')}}</th>
                                <th>{{__('image')}}</th>
                                <th>{{__('link')}}</th>
                                <th>{{__('status')}}</th>
                                <th>{{__('Spot_position')}}</th>
                                <th width="200px">{{__('Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($advertises))
                            @foreach ($advertises as $map=>$advertise)
                                <tr>
                                    
                                
                                  <td > <?php  echo ($id++)?> </td>
                                    <td class="Permission">{{isset($advertise['name'])?$advertise['name']:''}}</td>
                                    <td><img src="{{asset('/storage/doctor/'.$advertise->image)}}" width='100px' height='100px' ></td>
                                   <td>{{$advertise->link}}</td>
                                   
                                   <td>@if($advertise->status=='1')
                                    <button class="badge badge-pill badge-primary">Active</button>
                                    @else
                                  <button class="badge badge-pill badge-danger" >Not Active</button>
                                  @endif
                                </td>
                                

                                <td>{{$advertise->position}}</td>
                                <td class="Action">
                                        <span>
                                        
                                        <a href="#" data-url="{{ URL::to('edit/'.$advertise->id.'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Edit advertise')}}" class="edit-icon"><i class="fas fa-pencil-alt" title="Edit"></i></a>
                                           
                                                <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$advertise->id}}').submit();"><i class="fas fa-trash" title="Delete"></i></a>
                                                {!! Form::open(['method' => 'DELETE', 'route' => ['advertise.destroy', $advertise->id],'id'=>'delete-form-'.$advertise->id]) !!}
                                                {!! Form::close() !!}
                                           
                                        </span>
                                    </td>
                               
                                    
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
