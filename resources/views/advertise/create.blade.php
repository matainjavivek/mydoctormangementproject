<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
<div class="card bg-none card-box">
    <form class="pl-3 pr-3"  method="post" action="{{ route('advertise.store') }}" onsubmit="return validAuthUser()" name="myForm" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-12 form-group">
                <div>
                    <label class="form-control-label" for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name"/>
                    <span class="gu-hide" style="color: red;" id="errorname"></span>
                </div>

                <input type="file" id="image" name="image" class="@error('image') is-invalid @enderror form-control">
                    <label for="exampleInputEmail1">Please Select Image</label>
                    
                    @error('image')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                </div> 

                <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                        <div class="choose-file">
                            <label for="image">
                                <div>{{__('Choose file here')}}</div>
                                <input class="form-control" name="image" type="file" id="image" accept="image/*" data-filename="profile_update">
                            </label>
                            <p class="profile_update"></p>
                        </div>
                        @error('avatar')
                        <span class="invalid-feedback text-danger text-xs" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <span class="clearfix"></span>
                    <span class="text-xs text-muted">{{ __('Please upload a valid image file. Size of image should not be more than 2MB.')}}</span>
                </div>


        </div>
 <div>
                    <label class="form-control-label" for="phone">Link</label>
                    <input type="url" class="form-control"    name="status"/>
                    <span class="gu-hide" style="color: red;" id="errorphone"></span>
                </div>
              
                <div>
                    <label class="form-control-label" for="phone">status</label>
                    <input type="text" class="form-control"   id="status_id" name="status"/>
                    <span class="gu-hide" style="color: red;" id="errorphone"></span>
                </div>
                <!-- <div>
                    <label class="form-control-label" for="phone">Spot Position</label>
                    <input type="text" class="form-control"   id="position_id" name="position"/>
                    <span class="gu-hide" style="color: red;" id="errorphone"></span>
                </div> -->
                <div class="mb-3">
    <label for="exampleInputSpot" class="form-control-label">Select Spot Position</label>
    <select name="position" id="position" class="form-control">
      <option value="">Select Position</option>
      <option value="kolkata">Kolkata</option>
      <option value="Delhi">Delhi</option>
      <option value="Hydrabad">Hydrabad</option>
</div>
                <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>

        </div>
        
        <div class="row">
            <div class="col-12 form-group">
         
                  
            
            
                
              
            </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>
</div>
