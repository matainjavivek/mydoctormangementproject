<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" >
<div class="card bg-none card-box">
    <form class="pl-3 pr-3" method="post" action="{{ route('zone.store') }}" onsubmit="return validZone()" name="myForm"  enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-12 form-group">
         
                  
            
            <div>
                <label class="form-control-label" for="zone">{{__('messages.Zone')}}</label>
                <input type="text" class="form-control" id="zone" name="zone" placeholder="Enter location" />
                <span class="gu-hide" style="color: red;" id="errorzone">{{__('validation.zone_required')}}</span>
                </div>


                <div id="map" style="width: 100%; height: 400px;top:4px"></div>
                    <ul class="geo-data">
                        <li>Zone: <span id="location"></span></li>
                        <li>Country: <span id="country"></span></li>
                        <li>Coordinates(x,y): <span id="coordinates"></span></li>
                        

                    </ul>
                    <input type="hidden" id="zone_coordinates" name='coordinates'/>
                </div>
                

                
                <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
                <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
            </div>
        </div>
    </form>
    <script src="{{asset('assets/js/user.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAwQ3Gm7MIQ61LSG4u2s6XBuZyLZMj47CE&libraries=drawing" async defer></script>
    <script src="drawmap.js"></script>
</div>
