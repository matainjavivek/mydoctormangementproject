@extends('layouts.admin')

@section('title')
    {{ __('Manage Zone') }}
@endsection

 @section('action-button')
    <div class="all-button-box row d-flex justify-content-end">

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a  class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('Create Zone')}}" data-url="{{route('zone.create')}}">
                    <i class="fas fa-plus"></i> {{__('Add')}}
                </a>
            </div> 


    </div>
@endsection 

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped data-table-order-desc">
                            <thead>
                            <tr>
                                <th>{{__('Id')}}</th>
                                <th>{{__('Zone')}}</th>
                                <th>{{__('Coordinates')}}</th>
                                <th width="200px">{{__('Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($zones))
                            @foreach ($zones as $map=>$zone)
                                <tr>
                                <td class="Permission">{{isset($zone['id'])?$polygon['id']:''}}</td>
                                    <td class="Permission">{{isset($zone['zone'])?$zone['zone']:''}}</td>
                                    <td class="Permission">{{isset($zone['coordinates'])?$zone['coordinates']:''}}</td>
                                   
                                    

                                    <td class="Action">
                                        <span>
                                        
                                        <a href="#" class="delete-icon mx-2"  data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$zone['id']}}').submit();"><i class="fas fa-trash" title="Delete"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['zone.destroy', $zone['id']],'id'=>'delete-form-'.$zone['id']]) !!}
                                            {!! Form::close() !!}

                                         </span>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
