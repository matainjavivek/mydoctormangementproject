@extends('layouts.admin')

@section('title')
    {{ __('messages.Dashboard') }}
@endsection

@push('head')
   {{--  @if($calenderTasks)
        <link rel="stylesheet" href="{{asset('assets/libs/fullcalendar/dist/fullcalendar.min.css')}}">
    @endif --}}
@endpush
@push('script')
    <script src="{{ asset('assets/js/chart.min.js') }}"></script>
    {{-- @if($calenderTasks)
        <script src="{{ asset('assets/libs/fullcalendar/dist/fullcalendar.min.js') }}"></script>
    @endif --}}


@endpush
@section('content')
    <div class="row">
        {{-- @if(!empty($arrErr)) --}}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 {{-- @if(!empty($arrErr['system']))
                    <div class="alert alert-danger text-xs">
                         {{ __('are required in') }} <a href="{{ route('settings') }}" class=""><u> {{ __('System Setting') }}</u></a>
                    </div>
                @endif --}}
                {{-- @if(!empty($arrErr['user']))
                    <div class="alert alert-danger text-xs">
                        {{ $arrErr['user'] }}
                         <a href="{{ route('users') }}" class=""><u>{{ __('here') }}</u></a>
                    </div>
                @endif
               @if(!empty($arrErr['role']))
                    <div class="alert alert-danger text-xs">
                        {{ $arrErr['role'] }} <a href="{{ route('roles.index') }}" class=""><u>{{ __('here') }}</u></a>
                    </div>
                @endif --}}
            </div>
        {{-- @endif --}}
    </div>

    <div class="row">
        {{-- @php
            $class = '';
            // if(count($arrCount) < 4)
            // {
            //     $class = 'col-lg-4 col-md-4';
            // }
            // else
            // {
            //     $class = 'col-lg-3 col-md-3';
            // }
            // $arrCount['owner']['total']=0;
            // $arrCount['owner'] = 0;
            // $arrCount['owner']['owner'] =0;
        @endphp --}}
        {{-- @if(isset($arrCount['owner'])) --}}
            {{-- <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="card card-box"> --}}
                    {{-- <div class="left-card"> --}}
                        {{-- <div class="icon-box bg-warning"><i class="fas fa-dolly"></i></div> --}}
                        {{-- <h4>{{ __('Jumlah Pesanan') }}</h4> --}}
                    {{-- </div> --}}
                    {{-- <div class="number-icon text-center">56</div> --}}
                    {{-- @if(\Auth::user()->type == 'Super Admin') --}}
                    {{-- @php $count=0; @endphp
                    @foreach ($orders as $order)
                    @php $count=$count+$order->number_of_tablet; @endphp
                    @endforeach --}}
                        {{-- <div class="user-text"> --}}
                            {{-- <h5>{{__('Jumlah Tablet')}} : <span class="text-dark text-sm">234</span></h5> --}}
                        {{-- </div> --}}
                    {{-- @endif --}}
                    {{-- <img src="{{('assets/img/dot-icon.png')}}" class="dotted-icon"> --}}
                    
                {{-- </div>
            </div> --}}

            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="card card-box">
                    <div class="left-card">
                 
                        <div class="icon-box bg-primary"><i class="fa-solid fa-user-doctor"></i></div>
                        <h4>{{ __('Total Doctor') }}</h4>
                    </div>
                    <div class="number-icon text-center">{{$total_doctors}}</div>
                    {{-- @if(\Auth::user()->type == 'Super Admin')
                        <div class="user-text">
                            <h5>{{__('Paid Users')}} : <span class="text-dark text-sm"></span></h5>
                        </div>
                    @endif --}}
                    <img src="{{('assets/img/dot-icon.png')}}" class="dotted-icon">
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="card card-box">
                    <div class="left-card">
                 
                        <div class="icon-box bg-primary"><i class="fa-solid fa-house-chimney-medical"></i></div>
                        <h4>{{ __('Total Clinic') }}</h4>
                    </div>
                    <div class="number-icon text-center">{{$total_clinics}}</div>
                    {{-- @if(\Auth::user()->type == 'Super Admin')
                        <div class="user-text">
                            <h5>{{__('Paid Users')}} : <span class="text-dark text-sm"></span></h5>
                        </div>

                    @endif --}}
                    <img src="{{('assets/img/dot-icon.png')}}" class="dotted-icon">
                </div>
            </div>
            {{-- <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="card card-box">
                    <div class="left-card">
                        <div class="icon-box bg-primary"><i class="fa-solid fa-user-plus"></i></div>
                        <h4>{{ __('Total Agen') }}</h4>
                    </div>
                    <div class="number-icon text-center">90</div>
                    {{-- @if(\Auth::user()->type == 'Super Admin')
                        <div class="user-text">
                            <h5>{{__('Paid Users')}} : <span class="text-dark text-sm"></span></h5>
                        </div>

                    @endif --}}
                    {{-- <img src="{{('assets/img/dot-icon.png')}}" class="dotted-icon">
                </div> --}}
           {{-- </div> --}}
        {{-- @endif --}}


        {{-- @if(isset($arrCount['order'])) --}}

        {{-- @endif --}}

       {{-- @if(isset($arrCount['plan']))
            <div class="{{ $class }} col-sm-6">
                <div class="card card-box">
                    <div class="left-card">
                        <div class="icon-box bg-success"><i class="fas fa-award"></i></div>
                        <h4>{{ __('Total Plan') }}</h4>
                    </div>
                    <div class="number-icon text-center">{{ $arrCount['plan']['plan'] }}</div>
                    @if(\Auth::user()->type == 'Super Admin')
                        <div class="user-text">
                            <h5>{{__('Most purchase plan')}} : <span class="text-dark text-sm">{{ ($arrCount['plan']['total']) ? $arrCount['plan']['total']->name : '-' }}</span></h5>
                        </div>
                    @endif
                    <img src="{{('assets/img/dot-icon.png')}}" class="dotted-icon">
                </div>
            </div>
        @endif

        @if(isset($arrCount['client']))
            <div class="{{ $class }} col-sm-6">
                <div class="card card-box">
                    <div class="left-card">
                        <div class="icon-box bg-primary"><i class="fas fa-user"></i></div>
                        <h4>{{ __('Total Client') }}</h4>
                    </div>
                    <div class="number-icon">{{ $arrCount['client'] }}</div>
                    <img src="{{('assets/img/dot-icon.png')}}" class="dotted-icon">
                </div>
            </div>
        @endif

        @if(isset($arrCount['user']))
            <div class="{{ $class }} col-sm-6">
                <div class="card card-box">
                    <div class="left-card">
                        <div class="icon-box bg-danger"><i class="fas fa-users"></i></div>
                        <h4>{{ __('Total User') }}</h4>
                    </div>
                    <div class="number-icon">{{ $arrCount['user'] }}</div>
                    <img src="{{('assets/img/dot-icon.png')}}" class="dotted-icon">
                </div>
            </div>
        @endif

        @if(isset($arrCount['deal']))
            <div class="{{ $class }} col-sm-6">
                <div class="card card-box">
                    <div class="left-card">
                        <div class="icon-box bg-warning"><i class="fas fa-handshake"></i></div>
                        <h4>{{ __('Total Deal') }}</h4>
                    </div>
                    <div class="number-icon">{{ $arrCount['deal'] }}</div>
                    <img src="{{('assets/img/dot-icon.png')}}" class="dotted-icon">
                </div>
            </div>
        @endif

        @if(isset($arrCount['invoice']))
            <div class="{{ $class }} col-sm-6">
                <div class="card card-box">
                    <div class="left-card">
                        <div class="icon-box bg-success"><i class="fas fa-file"></i></div>
                        <h4>{{ __('Total Invoice') }}</h4>
                    </div>
                    <div class="number-icon">{{ $arrCount['invoice'] }}</div>
                    <img src="{{('assets/img/dot-icon.png')}}" class="dotted-icon">
                </div>
            </div>
        @endif --}}


    </div>



    <div class="table-responsive">

          {{-- @if($users[0]->type =='doctor') { --}}
            <table class="col-lg-12 col-md-3 col-sm-6">
            <h5>Doctors details</h5>
         <thead>
                            <tr>
                                <th>{{__('messages.Name')}}</th>
                                <!-- <th>{{__('messages.City')}}</th>
                                <th>{{__('messages.Province')}}</th> -->
                                <th>{{__('Specialist')}}</th>
                                <th>{{__('Registration Code')}}</th>
                                 <th>{{__('messages.Address')}}</th>
                                <th>{{__('Image')}}</th>

                                <th>{{__('messages.Hospital Name')}}</th>
                               
                               
                                
                            </tr>
                            </thead>

                            <tbody>
                            @if (isset($doctors))
                            @foreach ($doctors as $key=>$doctor)
                                <tr>
                                   
                                    <td class="Permission">{{isset($doctor['name'])?$doctor['name']:''}}</td> 
                                    <td class="Permission">{{isset($doctor->CategoryName->name)?$doctor->CategoryName->name:''}}</td>
                                    <td><p class= "code" id="copycode-{{$doctor['id']}}">{{isset($doctor['referral_code'])?$doctor['referral_code']:''}} </p><a class= "button copy-button"  href="#"  id="copy-{{$doctor['id']}}" onclick="CopyToClipboard('{{$doctor['id']}}');return false;">Copy</a></td>
                                     <td class="Permission">{{isset($doctor['address'])?$doctor['address']:''}}</td> 
                                    <td class="Permission"> <img alt="image" class="mr-3 rounded-circle" width="50" height="50" src="@if($doctor['image']){{asset('/storage/doctor/'.$doctor['image'])}} @else {{asset('assets/img/avatar/avatar-1.png')}} @endif"></td>
                                    
                                    <td class="Permission">{{isset($doctor['hospital_name'])?$doctor['hospital_name']:''}}</td>
                                   
                                   
                                    </tr>
                            @endforeach
                            @endif
                            </tbody>
                  </table>      
                </div>
                {{-- @elseif($users->type =='clinic') { --}}
                <div class="table-responsive">
         <table class="col-lg-12 col-md-3 col-sm-6">
                
                <h5>Clinics details</h5>
                 <thead>
                            <tr>
                                <th>{{__('messages.shop_name')}}</th>
                                {{-- <th>{{__('messages.email')}}</th> --}}
                                <th>{{__('messages.mobile_number')}}</th>
                                <th>{{__('messages.shop_phone_number')}}</th>
                                
                                <th>{{__('messages.Address')}}</th>
                                <th>{{__('Image')}}</th>
                                 <th>{{__('messages.Status')}}</th>
                               
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($clinics))
                            @foreach ($clinics as $key=>$clinic)
                                <tr>

                                    <td class="Permission">{{isset($clinic['name'])?$clinic['name']:''}}</td>
                                    {{-- <td class="Permission">{{isset($clinic['email'])?$clinic['email']:''}}</td> --}}
                                    <td class="Permission">{{isset($clinic['phone'])?$clinic['phone']:''}}</td>
                                    <td class="Permission">{{isset($clinic['shop_phone'])?$clinic['shop_phone']:''}}</td>
                                    
                                    <td class="Permission">{{isset($clinic['address'])?$clinic['address']:''}}</td>

                                    <td class="Permission"> <img alt="image" class="mr-3 rounded-circle" width="50" height="50" src="@if($clinic['image']){{asset('/storage/clinic/'.$clinic['image'])}} @else {{asset('assets/img/avatar/avatar-1.png')}} @endif"></td>
                                       
                                    
                                        <td>
                                        {{-- {{$clinic->status}} --}}
                                        @if(isset($clinic->status))
                                        @if($clinic->status == 1)
                                            <span class="badge badge-pill badge-primary">{{__(\App\Models\User::$statues[$clinic->status]) }}</span>
                                        @elseif($clinic->status == 0)
                                            <span class="badge badge-pill badge-danger">{{__(\App\Models\User::$statues[$clinic->status]) }}</span>
                                        
                                        @endif
                                         @endif
                                        @if(! isset($clinic->status))
                                              <span class="badge badge-pill badge-primary">{{__(\App\Models\User::$statues[0]) }}</span>  
                                        @endif
                                        {{-- @php
                                        dd(\App\Models\User::$statues[0]);
                                        @endphp --}}
                                    </td>
                                   
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                         </table>  
                
           
        
</div>


@endsection




