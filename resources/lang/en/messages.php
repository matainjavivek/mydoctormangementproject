<?php
 
// lang/IN/messages.php
 
return [
   
    'Name'=>'Name',
    'City'=>'City',
    'Province'=>'Province',
    'Phone'=>'Phone',
    'Description'=>'Description',
    'price'=>'price',
    'Address'=>'Address',
    'E-Mail Address'=>'E-Mail Address',
    'Password'=>'Password',
    'Job Title'=>'Job Title',
    'Role'=>'Role',
    'Hopital Name'=>'Hopital Name',
    "Dashboard"=>'Dashboard',
    
    'Agent'=>'Agent',
    'Doctor'=>'Doctor',
    'Chamber'=>'Chamber',
  
    'Order'=>'Order',
    'Users'=>'Users',
    'Roles'=>'Roles',
    'System Settings'=>'System Settings',
    'Report'=>'Report',
    'My Profile'=>'My Profile',
    
    
    'Referral Code'=>'Referral Code',
    'Url'=>'Url',
    'QR'=>'QR',
    'Action'=>'Action',
    'Add'=>'Add',
    'number'=>'number',
    'Create chamber'=>'Create chamber',
   
    
    
    'Manage Chamber'=>'Manage Chamber',
    
    'Hospital Name'=>'Hospital Name',

    'Pharmacy'=>'Pharmacy',
    'Manage Pharmacy'=>'Manage Pharmacy',
    'Create Pharmacy'=>'Create Pharmacy',
    'Edit Pharmacy'=>'Edit Pharmacy',
    
    'Manage doctor'=>'Manage Doctor',
    'Create doctor'=>'Create Doctor',
    'Edit doctor'=>'Edit Doctor',


    'Manage Agent'=>'Manage Agent',
    'Create agent'=>'Create Agent',
    'Edit agent'=>'Edit Agent',

    'Create clinic'=>'Create Clinic',
    'Manage Clinic'=>'Manage Clinic',
    'Edit clinic'=>'Edit Clinic',

    'Medicine'=>'Medicine',
    'Manage Medicine'=>'Manage Medicine',
    'Add medicine'=>'Create medicine',

    
    'Manage Category'=>'Manage Category',
    'Edit category'=>'Edit Category',
    'Create Category'=>'Create Category',
    
    'Order id'=>'Order id',
    'Date'=>'Date',
    'Doctor name'=>'Doctor name',
    'Referral name'=>'Referral name',
    'Phone number'=>'Phone number',
    'mobile_number'=>'Mobile number',
    'shop_phone_number'=>'Shop phone number',
    'Tablets'=>'Tablets',
    'C.R (RP)'=>'C.R (RP)',
    'Commission (RP)'=>'Komisi (RP)',
    'Status'=>'Status',
    'Manage Order'=>'Manage Order',
    'Manage Users'=>'Manage Users',
    'Edit User'=>'Edit User',
    'Edit chamber'=>'Edit chamber',
    'Manage Roles'=>'Manage Roles',
    'Permissions'=>'Permissions',
    'Create Role'=>'Create Role',
    'Role Name'=>'Role Name',
    'Assign Permissions'=>'Assign Permissions',
    'Module'=>'Module',
    'Settings '=>'Settings',
    'Dealy Report'=>'Dealy Report',
    'Daily'=>'Daily',
    'Monthly'=>'Monthly',
    'Search By Date'=>'Search By Date',
    'Settings'=>'Settings',
    'Create User'=>'Create User',
    'Order Create'=>'Order Create',
    'Referral code (Optional)'=>'Referral code (Optional)',
    'Number of tablets'=>'Number of tablets',
    'Full name'=>'Full name',
    'Select referral name'=>'Select referral name',
    'Order Edit'=>'Order Edit',
    'Select status'=>'Select status',
    'COMMISSION RATE (RP)'=>'COMMISSION RATE (RP)',
    'Edit Status'=>'Edit Status',
    'clinic'=>'Clinic',
    'shop_name'=>'Shop name',
    
    'email'=>'Email',
    'Specialist'=>'Specialist'



   
];