var base_url = window.location.origin;
var swiper = new Swiper(".mySwiper", {
    autoplay: true,
  pagination: {
    el: ".swiper-pagination",
    dynamicBullets: true,
  },
});

$('.digit-group').find('input').each(function () {
    $(this).attr('maxlength', 1);
    $(this).on('keyup', function (e) {
        var parent = $($(this).parent());

        if (e.keyCode === 8 || e.keyCode === 37) {
            var prev = parent.find('input#' + $(this).data('previous'));

            if (prev.length) {
                $(prev).select();
            }
        } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
            var next = parent.find('input#' + $(this).data('next'));

            if (next.length) {
                $(next).select();
            } else {
                if (parent.data('autosubmit')) {
                    parent.submit();
                }
            }
        }
    });
});





const firebaseConfig = {
    apiKey: "AIzaSyBkiaGGQWA4g8XZVMEUR1csefy1jRwoA5o",
    authDomain: "sleep-9949e.firebaseapp.com",
    projectId: "sleep-9949e",
    storageBucket: "sleep-9949e.appspot.com",
    messagingSenderId: "233754733458",
    appId: "1:233754733458:web:ee787275dd534b18762f4b",
    measurementId: "G-J4TWC43LH0"
};

firebase.initializeApp(firebaseConfig);

var phoneNumber=null;
    window.onload = function () {
        render();
    };

    function render() {
       window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('login_submit', {
  'size': 'invisible',
  'callback': (response) => {
    // reCAPTCHA solved, allow signInWithPhoneNumber.
   // onSignInSubmit();

   // alert("llll");
   console.log("Captha solve");

 
  }
});
    }

    function sendFirebasePhoneOTP() {
       
        

      // $('#login_submit').click(function() { alert("lllooo"); return false; }); 
      // $('#login_submit').prop("onclick", null).off("click");

        var countryCode = $("#country_code").val();
        var phoneNumber =countryCode+$("#phone").val();
        // let y = document.forms["myForm"]["mobile"].value;
        var phone =$("#phone").val();
        if (phone == "") {
            var element = document.getElementById("phone");
            element.classList.add("radbox");
            var element = document.getElementById("errorphone");
            element.classList.remove("gu-hide");
            //alert("Name must be filled out");
            return false;
        }else{
            var element = document.getElementById("phone");
            element.classList.remove("radbox");
            var element = document.getElementById("errorphone");
            element.classList.add("gu-hide");
        }
        console.log($.type(phoneNumber));
        console.log(phoneNumber);

        $("#login_submit").text("Please Wait ..."); 

        $("#phone_number").text(phoneNumber);

        captcha =window.recaptchaVerifier;

        

        // sessionStorage.setItem("captcha", captcha);
        // console.log(captcha1 );
        firebase.auth().signInWithPhoneNumber(phoneNumber,  window.recaptchaVerifier)
        .then(function (confirmationResult) {
            window.confirmationResult = confirmationResult;
            coderesult = confirmationResult;
            $("#otp_send").addClass("gu-hide");
            $("#otp_receive").removeClass("gu-hide");
            $("#authSuccess").text("Message sent");
            $("#authSuccess").show();
        }).catch(function (error) {
            $("#showError").text("Someting is wrong try again");
            $("#showError").show();
             $("#login_submit").text("Submit");
            //  $('#login_submit').prop("onclick", sendFirebasePhoneOTP());
            //$("login_submit").on("click", function() { sendFirebasePhoneOTP(); });
        });

    }
    function resendFirebasePhoneOTP() {
        // var captcha = sessionStorage.getItem("captcha");
        var countryCode = $("#country_code").val();
        var phoneNumber =countryCode+$("#phone").val();
        console.log(firebase.auth());
        captcha1.signInWithPhoneNumber(phoneNumber, captcha)
        .then(function (confirmationResult) {
            window.confirmationResult = confirmationResult;
            coderesult = confirmationResult;
            console.log(phoneNumber);
            $("#authSuccess").text("Message sent");
            $("#authSuccess").show();
        }).catch(function (error) {
            console.log(phoneNumber);
            $("#showError").text(error.message);
            $("#showError").show();
        });
        console.log('captcha');
        console.log(captcha);
    }
    function verifyPhoneNumber() {
        $("#verify_button").text("Please Wait ...");
        var code2 = $("#digit-2").val();
        var code3 = $("#digit-3").val();
        var code4 = $("#digit-4").val();
        var code1 = $("#digit-1").val();
        var code5 = $("#digit-5").val();
        var code6 = $("#digit-6").val();
        var code =code1+code2+code3+code4+code5+code6;
       

        if(code == "" ||  code == null )
        {
            $("#showError_verify").text("Please Enter OTP");
            $("#showError_verify").show();
            $("#verify_button").text("Verify");
            return false;

            
        }
        console.log(code);
        coderesult.confirm(code).then(function (res) {
            var url=base_url+'/session';
            var countryCode = $("#country_code").val();
            var phoneNumber =countryCode+$("#phone").val();
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                    url: url,
                    data:{'phoneNumber':phoneNumber,},
                    type:"POST",
                    success: function (response) {
                        var route = base_url+'/userdetils';
                        window.location.href =route;
                    },
                });



            $("#otpSuccess").text("Auth successful");
            $("#otpSuccess").show();
        }).catch(function (error) {
            $("#showError").text(error.message);
            $("#showError").show();
             $("#verify_button").text("Verify");
        });
    }

    $('.digit-group').find('input').each(function () {
        $(this).attr('maxlength', 1);
        $(this).on('keyup', function (e) {
            var parent = $($(this).parent());

            if (e.keyCode === 8 || e.keyCode === 37) {
                var prev = parent.find('input#' + $(this).data('previous'));

                if (prev.length) {
                    $(prev).select();
                }
            } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                var next = parent.find('input#' + $(this).data('next'));

                if (next.length) {
                    $(next).select();
                } else {
                    if (parent.data('autosubmit')) {
                        parent.submit();
                    }
                }
            }
        });
    });
