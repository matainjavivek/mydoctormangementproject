function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {

        });
        var input = document.getElementById('address');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });
        infowindow.close();
            marker.setVisible(false);
            autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
            $("#locationsearch").removeClass("gu-hide");
            $('#lat').attr("value",place.geometry.location.lat());
            $('#lon').attr("value",place.geometry.location.lng());
            $('#city').attr("value",place.formatted_address);
            });
    }

    var swiper = new Swiper(".mySwiper", {
        slidesPerView: "auto",
        spaceBetween: 30,
        slidesPerView: 1.5,
        autoplay: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
    });
