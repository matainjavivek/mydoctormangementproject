<?php

use App\Http\Controllers\Api\CategoryApiController;
use App\Http\Controllers\Api\DegreeApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ClinicApiController;
use App\Http\Controllers\Api\DoctorApiController;
use App\Http\Controllers\Api\AgentApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
    
});*/
Route::prefix('clinic')->group(function() {
    Route::post('/register', [ClinicApiController::class, 'register'])->name('api.clinic.register');
    Route::post('/login', [ClinicApiController::class, 'login'])->name('api.clinic.login');

    Route::group(['middleware' => 'auth:api'], function(){
        
    Route::post('/store', [ClinicApiController::class, 'store'])->name('api.clinic.store');
    
    Route::post('/doctor/add', [ClinicApiController::class, 'doctorAdd'])->name('api.clinic.doctorAdd');
    Route::post('/doctor/time', [ClinicApiController::class, 'doctorTime'])->name('api.clinic.doctorTime');
    Route::post('/doctors', [ClinicApiController::class, 'clinicDoctors'])->name('api.clinic.doctors');
    Route::post('/doctors/destroy', [ClinicApiController::class, 'clinicDoctorsDestroy'])->name('api.clinic.doctors.destroy');
    Route::post('/doctors/schedule', [ClinicApiController::class, 'doctorSchedule'])->name('api.clinic.doctors.schedule');
    Route::post('/doctors/schedule/destroy', [ClinicApiController::class, 'doctorScheduleDestroy'])->name('api.clinic.doctors.schedule.destroy');
    Route::post('/image', [ClinicApiController::class, 'clinicImageSave'])->name('api.clinic.image');
    Route::post('/edit', [ClinicApiController::class, 'edit'])->name('api.clinic.edit');
    //Route::post('/update',[ClinicApiContoller::class,'update'])->name('api.clinic.updatte');
    Route::post('/destroy', [ClinicApiController::class, 'destroy'])->name('api.clinic.destroy');

    Route::post('/doctors/schedules/add', [ClinicApiController::class, 'addSchedules'])->name('api.clinic.doctors.addschedules');
    //Route::post('/doctor/add_all',[ClinicApiController::class, 'addAll'])->name('clinic.doctor.addAll');
});
Route::post('/distance', [ClinicApiController::class, 'closestCarriers'])->name('api.user.clinic.distance');
Route::post('/user/doctors', [ClinicApiController::class, 'userClinicDoctors'])->name('api.user.clinic.doctors');



});

Route::prefix('doctors')->group(function() {

    
    Route::group(['middleware' => 'auth:api'], function(){
    Route::post('/', [DoctorApiController::class, 'doctors'])->name('api.doctor.api.register');
    Route::post('/create', [DoctorApiController::class, 'create'])->name('api.doctor.api.create');
    Route::post('/shop/create/doctor', [DoctorApiController::class, 'DoctorCreateByShop'])->name('doctor.api.DoctorCreateByShop');
   


    
});
//user api
Route::post('/search', [DoctorApiController::class, 'search'])->name('doctor.api.search');
Route::post('/search/doctor/id', [DoctorApiController::class, 'searchByDoctorId'])->name('doctor.api.search.id');
Route::post('/clinic/online',[DoctorApiController::class,'doctorOnline'])->name('api.doctor/clinic/online');
});

Route::prefix('category')->group(function() {
    // Route::group(['middleware' => 'auth:api'], function(){
    Route::post('/', [CategoryApiController::class, 'category'])->name('api.category');
    // Route::post('/create', [CategoryApiController::class, 'category'])->name('category');
// });
});


Route::prefix('degree')->group(function() {
    // Route::group(['middleware' => 'auth:api'], function(){
    Route::post('/', [DegreeApiController::class, 'degree'])->name('api.degree');
});


Route::prefix('agents')->group(function() {
    Route::post('/login', [AgentApiController::class, 'login'])->name('api.agent.login');
    Route::post('/create', [AgentApiController::class, 'create'])->name('agent.api.create');
    Route::post('/', [AgentApiController::class, 'index'])->name('api.agent');
    Route::group(['middleware' => 'auth:api'], function(){

   
});
});

