<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ZoneController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::group(['prefix' => 'admin'], function () {
	
	Route::delete('delete/{id}', 'AdvertiseController@destroy')->name('advertise.destroy');
	Route::get('edit/{id}', 'AdvertiseController@edit')->name('advertise.edit');
	Route::prefix('advertise')->group(function() {
		Route::get('/', 'AdvertiseController@index')->name('advertise.index');
		Route::get('/create', 'AdvertiseController@create')->name('advertise.create');
		Route::post('/store', 'AdvertiseController@store')->name('advertise.store');
	   Route::post('update/{id}', 'AdvertiseController@destroy')->name('adveritse.update');

	});
		
	Route::prefix('pharmacy')->group(function() {
        Route::get('/', 'AdvertiseControlle@index')->name('pharmacy');
        Route::get('/create', 'AdvertiseControlle@create')->name('pharmacy.create');
        Route::post('/store', 'AdvertiseControlle@store')->name('pharmacy.store');
        Route::get('/{id}/edit', 'AdvertiseControlle@edit')->name('pharmacy.edit');
        Route::post('/update', 'AdvertiseControlle@update')->name('pharmacy.update');
        Route::delete('/{id}/destroy', 'AdvertiseControlle@destroy')->name('pharmacy.destroy');
        Route::get('/download/{id}/{unique}', 'AdvertiseControlle@pdfview')->name('pharmacy.download');
        Route::get('/qrview/{id}/{unique}', 'AdvertiseControlle@qrview')->name('pharmacy.qrview');

    });
	// Route::get('/', function () {
	//     return view('welcome');
	// });

	require __DIR__.'/auth.php';


	Route::get('/register', function () {
	    return redirect('/login');
	});

	Route::get('/', function(){
	   return abort(404);
	});


    Route::get('/', ['as' => 'homeguest','uses' => 'HomeController@index',])->middleware(['XSS']);
	Route::get('/home', ['as' => 'home','uses' => 'HomeController@index',])->middleware(['auth','XSS',]);
	Route::get('/check', 'HomeController@check')->middleware(['auth','XSS',]);
	Route::get('/profile', ['as' => 'profile','uses' => 'UserController@profile',])->middleware(['auth','XSS',]);
	Route::post('/profile', ['as' => 'update.profile','uses' => 'UserController@updateProfile',])->middleware(['auth','XSS',]);
	Route::post('/profile/password', ['as' => 'update.password','uses' => 'UserController@updatePassword',])->middleware(['auth','XSS',]);

	Route::delete('/profile', ['as' => 'delete.avatar','uses' => 'UserController@deleteAvatar',])->middleware(['auth','XSS',]);

	Route::get('/users', ['as' => 'users','uses' => 'UserController@index',])->middleware(['auth','XSS',]);
	Route::post('/users', ['as' => 'users.store','uses' => 'UserController@store',])->middleware(['auth','XSS',]);
	Route::get('/users/create', ['as' => 'users.create','uses' => 'UserController@create',])->middleware(['auth','XSS',]);
	Route::get('/users/edit/{id}', ['as' => 'users.edit','uses' => 'UserController@edit',])->middleware(['auth','XSS',]);
	Route::get('/users/{id}',['as' => 'users.show','uses' =>'UserController@show'])->middleware(['auth','XSS']);
	Route::post('/users/{id}', ['as' => 'users.update','uses' => 'UserController@update',])->middleware(['auth','XSS',]);
	Route::delete('/users/{id}', ['as' => 'users.destroy','uses' => 'UserController@destroy',])->middleware(['auth','XSS',]);
	Route::post('/userCreateFromCsv', ['as' => 'userCreateFromCsv','uses' => 'UserController@userCreateFromCsv',])->middleware(['auth','XSS',]);
	Route::post('/profile/userpassword', ['as' => 'update.userpassword','uses' => 'UserController@userpassword',])->middleware(['auth','XSS',]);

	Route::get('/test', ['as' => 'test.email','uses' => 'SettingsController@testEmail',])->middleware(['auth','XSS',]);
	Route::post('/test/send', ['as' => 'test.email.send','uses' => 'SettingsController@testEmailSend',])->middleware(['auth','XSS',]);
	Route::post('/payment-settings', 'SettingsController@adminPaymentSettings')->name('payment.settings')->middleware(['auth','XSS']);

	Route::post('/template-setting',['as' => 'template.setting','uses' =>'SettingsController@saveTemplateSettings'])->middleware(['auth','XSS']);
	Route::get('/invoices/preview/{template}/{color}',['as' => 'invoice.preview','uses' =>'InvoiceController@previewInvoice']);
Route::get('/invoices/preview/{template}/{color}',['as' => 'invoice.preview','uses' =>'InvoiceController@previewInvoice']);



Route::post('/site-settings', ['as' => 'site.settings.store','uses' => 'SettingsController@site_setting',])->middleware(['auth','XSS',]);
	Route::get('/settings', ['as' => 'settings','uses' => 'SettingsController@index',])->middleware(['auth','XSS',]);
	Route::post('/settings', ['as' => 'settings.store','uses' => 'SettingsController@store',])->middleware(['auth','XSS',]);

	Route::get('/{uid}/notification/seen',['as' => 'notification.seen','uses' =>'UserController@notificationSeen']);


	/* fake Router */
	Route::post('/message_data', 'SettingsController@savePaymentSettings')->name('message.data')->middleware(['auth','XSS']);

	Route::post('/message_seen', 'SettingsController@savePaymentSettings')->name('message.seen')->middleware(['auth','XSS']);
	//================================= Invoice Payment Gateways  ====================================//

	Route::get('/search',['as' => 'search.json','uses' =>'UserController@search']);

	Route::get('/invoices/payments', ['as' => 'invoices.payments','uses' => 'InvoiceController@payments',])->middleware(['auth','XSS',]);





	Route::resource('roles', 'RoleController');
	Route::resource('permissions', 'PermissionController');


    // Route::prefix('pharmacy')->group(function() {

	Route::prefix('pharmacy')->group(function() {
        Route::get('/', 'PharmacyController@index')->name('pharmacy');
        Route::get('/create', 'PharmacyController@create')->name('pharmacy.create');
        Route::post('/store', 'PharmacyController@store')->name('pharmacy.store');
        Route::get('/{id}/edit', 'PharmacyController@edit')->name('pharmacy.edit');
        Route::post('/update', 'PharmacyController@update')->name('pharmacy.update');
        Route::delete('/{id}/destroy', 'PharmacyController@destroy')->name('pharmacy.destroy');
        Route::get('/download/{id}/{unique}', 'PharmacyController@pdfview')->name('pharmacy.download');
        Route::get('/qrview/{id}/{unique}', 'PharmacyController@qrview')->name('pharmacy.qrview');

    });

    Route::prefix('doctor')->group(function() {
        Route::get('/', 'DoctorController@index')->name('doctor');
        Route::get('/create', 'DoctorController@create')->name('doctor.create');
        Route::post('/store', 'DoctorController@store')->name('doctor.store');
        Route::get('/{id}/edit', 'DoctorController@edit')->name('doctor.edit');
        Route::post('/update', 'DoctorController@update')->name('doctor.update');
        Route::delete('/{id}/destroy', 'DoctorController@destroy')->name('doctor.destroy');
        Route::get('/download', 'DoctorController@download')->name('doctor.download');
        Route::get('/qrview/{id}/{unique}', 'DoctorController@qrview')->name('doctor.qrview');
        Route::get('/chamber/{id}', 'DoctorController@chamber')->name('doctor.chamber');
        Route::post('/update/chamber', 'DoctorController@chamberUpdate')->name('doctor.update.chamber');
        Route::get('chamber/{id}/destroy', 'DoctorController@chamberDestroy')->name('doctor.destroy.chamber');

		// Route::get('/category/{id}', 'DoctorController@category')->name('doctor.category');
        // Route::post('/update/category', 'DoctorController@categoryUpdate')->name('doctor.update.category');
        // Route::get('category/{id}/destroy', 'DoctorController@categoryDestroy')->name('doctor.destroy.category');
		Route::get('/{id}', 'DoctorController@DoctorEducation')->name('doctor.education');
		Route::POST('/education', 'DoctorController@AddDoctorEducation')->name('doctor.education.add');
		Route::get('/education/{id}/destroy', 'DoctorController@DoctorEducationDestroy')->name('doctor.education.destroy');

    });

    Route::prefix('order')->group(function() {
        Route::get('/', 'MedicineOrderController@index')->name('order');

        Route::get('/add', 'MedicineOrderController@add')->name('order.add');

        Route::post('/update', 'MedicineOrderController@update')->name('order.update');
        Route::get('/{id}/edit', 'MedicineOrderController@edit')->name('order.edit');

        Route::get('/{id}/status', 'MedicineOrderController@status')->name('order.status');
        Route::post('/update/status', 'MedicineOrderController@statusUpdate')->name('order.status.update');
        Route::delete('/{id}/destroy', 'MedicineOrderController@destroy')->name('order.destroy');
        Route::get('/create/{referral_type}/{referral_code}', 'UserOrderController@index')->name('order.create');
        Route::get('/commission/{referral_code}/{type}', 'MedicineOrderController@commission')->name('order.commission');
        Route::post('/search', 'MedicineOrderController@search')->name('search.order');
        Route::post('/store', 'UserOrderController@store')->name('order.store');
        Route::get('/success/{referral_type?}/{referral_code?}', 'UserOrderController@success')->name('order.success');
        Route::get('/find/{referral_code?}', 'UserOrderController@UserFindByReferralCode')->name('order.find');
		

        Route::get('reports/plaintiffId', 'UserOrderController@plaintiffId')->name('reports.plaintiffId');

        Route::get('/search/order', 'MedicineOrderController@searchOrder')->name('order.search');
    });

    Route::get('/demo-search', 'MedicineOrderController@searchDemo');

    Route::get('/autocomplete', 'MedicineOrderController@autocomplete')->name('autocomplete');
    Route::get('/{url}/s', 'UserOrderController@url')->name('url');

	Route::prefix('agent')->group(function() {
		Route::get('/', 'AgentController@index')->name('agent');
		Route::get('/create', 'AgentController@create')->name('agent.create');
		Route::post('/store', 'AgentController@store')->name('agent.store');
		Route::get('/{id}/edit', 'AgentController@edit')->name('agent.edit');
		Route::post('/update', 'AgentController@update')->name('agent.update');
		Route::delete('/{id}/destroy', 'AgentController@destroy')->name('agent.destroy');
		Route::get('/download', 'AgentController@download')->name('agent.download');
        Route::get('/qrview/{id}/{unique}', 'AgentController@qrview')->name('agent.qrview');
	  Route::get('/commission/{id}', 'AgentController@commission')->name('agent.commission');

});
Route::prefix('report')->group(function() {
    Route::get('/search/{daily}', 'ReportController@index')->name('report');
    Route::get('/store', 'ReportController@search')->name('report.store');
});

		Route::get('/medicine', 'MedicineController@index')->name('medicine.index');
		Route::get('medicine/create', 'MedicineController@create')->name('medicine.create');
		Route::post('medicine/store', 'MedicineController@store')->name('medicine.store');
		Route::get('medicine/{id}/edit', 'MedicineController@edit')->name('medicine.edit');
		Route::post('medicine/update', 'MedicineController@update')->name('medicine.update');
		Route::delete('medicine/{id}/destroy', 'MedicineController@destroy')->name('medicine.destroy');

Route::get('/copy/{referral_type}/{referral_code}', 'MedicineOrderController@copy')->name('copy');

Route::get('/googlemap', 'PharmacyController@googlemap');


Route::post('/modifyUser', 'UserController@modifyUser')->name('user.modifyUser');
Route::post('/modifyPharmacy', 'UserController@modifyPharmacy')->name('user.modifyPharmacy');
Route::post('/modifyDoctor', 'UserController@modifyDoctor')->name('user.modifyDoctor');


// Route::prefix('front')->group(function() {
    Route::get('/log', 'FrontendUserController@customer')->name('authuser.customer');
Route::get('/authlogin', 'FrontendHomeController@index')->name('front.authlogin');
Route::post('/signup', 'FrontendHomeController@signup')->name('front.signup');
Route::get('/otp', 'FrontendHomeController@otp')->name('front.otp');
Route::get('/userdetils', 'FrontendHomeController@userdetils')->name('front.userdetils');
Route::post('/store', 'FrontendHomeController@store')->name('front.store');
Route::get('/customer/profile', 'FrontendHomeController@Customerprofile')->name('authuser.profile');
Route::get('/doctor/list', 'FrontendHomeController@doctorList')->name('authuser.doctorList');
Route::get('/closest/doctor', 'FrontendHomeController@closestCarriers')->name('front.closestCarriers');
Route::get('/logout', 'FrontendHomeController@logout')->name('front.logout');

// });
Route::get('/edit/{id}', 'FrontendHomeController@edit')->name('front.edit');
Route::post('/update', 'FrontendHomeController@update')->name('front.update');
Route::post('/session', 'FrontendHomeController@sessionadd')->name('front.session');
Route::get('phone-auth', 'FrontendHomeController@demo');

Route::get('/helplogin', 'FrontendUserController@helplogin')->name('authuser.helplogin');


Route::prefix('clinic')->group(function() {
Route::get('/', 'ClinicController@index')->name('clinic.index');
Route::get('/create', 'ClinicController@create')->name('clinic.create');
Route::post('/store', 'ClinicController@store')->name('clinic.store');
Route::get('/{id}/edit', 'ClinicController@edit')->name('clinic.edit');
Route::post('/update', 'ClinicController@update')->name('clinic.update');
Route::delete('/{id}/destroy', 'ClinicController@destroy')->name('clinic.destroy');
Route::get('/image/{id}', 'ClinicController@clinicImage')->name('clinic.image');
Route::post('/image/update', 'ClinicController@clinicImageSave')->name('clinic.image.update');
Route::get('/{id}/schedule', 'ClinicController@schedule')->name('clinic.schedule');
Route::post('/timetadle/add', 'ClinicController@timetadleAdd')->name('clinic.timetadle.add');
Route::get('/timetadle/{id}/destroy', 'ClinicController@doctordestroy')->name('clinic.doctor.destroy');
Route::get('/timetadle/{id}/doctor', 'ClinicController@doctorAdd')->name('clinic.timetadle.doctor');
Route::get('/{id}/schedule', 'ClinicController@schedule')->name('clinic.schedule');
Route::get('/{clinic}/schedule/{doctor}', 'ClinicController@doctorschedule')->name('clinic.doctor.schedule');
Route::post('doctor/schedule/store', 'ClinicController@doctorScheduleStore')->name('clinic.doctor.schedule.store');
Route::get('/schedule/{id}/destroy', 'ClinicController@doctorScheduleDestroy')->name('clinic.schedule.destroy');
Route::get('/detail/{id}', 'ClinicController@detail')->name('clinic.detail');
});

Route::prefix('category')->group(function() {
	Route::get('/', 'CategoryController@index')->name('category.index')->middleware(['guard']);
	Route::get('/create', 'CategoryController@create')->name('category.create')->middleware(['guard']);
	Route::post('/store', 'CategoryController@store')->name('category.store')->middleware(['guard']);
	Route::get('/{id}/edit', 'CategoryController@edit')->name('category.edit')->middleware(['guard']);
	Route::post('/update', 'CategoryController@update')->name('category.update')->middleware(['guard']);
	Route::delete('/{id}/destroy', 'CategoryController@destroy')->name('category.destroy')->middleware(['guard']);
	
	});
	Route::prefix('degree')->group(function() {
		
		Route::get('/', 'DegreeController@index')->name('degree.index')->middleware(['guard']);
		Route::get('/create', 'DegreeController@create')->name('degree.create')->middleware(['guard']);
		Route::post('/store', 'DegreeController@store')->name('degree.store')->middleware(['guard']);
		Route::get('/{id}/edit', 'DegreeController@edit')->name('degree.edit')->middleware(['guard']);
		Route::post('/update', 'DegreeController@update')->name('degree.update')->middleware(['guard']);
		Route::delete('/{id}/destroy', 'DegreeController@destroy')->name('degree.destroy')->middleware(['guard']);
		
		});
		Route::prefix('zone')->group(function() {
			Route::get('/', 'ZoneController@index')->name('zone.index');
			Route::get('/create', 'ZoneController@create')->name('zone.create');
			Route::post('/store', 'ZoneController@store')->name('zone.store');
			Route::delete('/{id}/destroy', 'ZoneController@destroy')->name('zone.destroy');});
		

			Route::prefix('spot')->group(function() {
				Route::get('/', 'SpotController@index')->name('spot.index');
				Route::get('/create', 'SpotController@create')->name('spot.create');
				Route::post('/store', 'SpotController@store')->name('spot.store');
			});


