<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToDoctorCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctor_categories', function (Blueprint $table) {
            $table->string('name')->nullable()->after('category_id');
            $table->string('description')->nullable()->after('category_id');
            $table->string('image')->nullable()->after('category_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_categories', function (Blueprint $table) {
            //
        });
    }
}
