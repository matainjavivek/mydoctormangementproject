<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserPhoneToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('city')->after('type')->nullable();
            $table->string('province')->after('city')->nullable();
            $table->string('phone')->after('province')->nullable();
            $table->string('referral_code')->after('phone')->nullable();
            $table->string('hospital_name')->after('referral_code')->nullable();
            $table->string('address')->after('hospital_name')->nullable();
            $table->string('short_url')->after('address')->nullable();
            $table->string('url')->after('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //

                $table->dropColumn('city');
                $table->dropColumn('province');
                $table->dropColumn('phone');
                $table->dropColumn('referral_code');
                $table->dropColumn('hospital_name');
                $table->dropColumn('address');
                $table->dropColumn('short_url');
                $table->dropColumn('url');

        });
    }
}
