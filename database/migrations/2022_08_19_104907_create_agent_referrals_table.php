<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('agent_referrals') ) {
            Schema::create('agent_referrals', function (Blueprint $table) {
                $table->id();
                $table->integer('clinic_id')->unsigned();
                $table->integer('agent_id')->unsigned();
                $table->integer('commission')->nullable()->default(0);
                $table->integer('status')->nullable()->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_referrals');
    }
}
