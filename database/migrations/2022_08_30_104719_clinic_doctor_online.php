<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClinicDoctorOnline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if ( !Schema::hasTable('clinic_doctor_online') ) {
            Schema::create('clinic_doctor_online', function (Blueprint $table) {
                $table->id();
                $table->integer('doctor_id');
                $table->string('clinic_id');
                $table->string('date');
                $table->string('online');
                $table->string('total_online');
                $table->timestamps();
            });
            }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('clinic_doctor_online');
    }
}
