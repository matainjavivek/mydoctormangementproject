<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('orders') ) {
            Schema::create('orders', function (Blueprint $table) {
                $table->bigIncrements('order_id');
                $table->unsignedBigInteger('customer_id')->nullable();
                $table->string('referral_code')->nullable();
                $table->unsignedBigInteger('agent_id')->nullable();
                $table->unsignedBigInteger('pharmacy_id')->nullable();
                $table->unsignedBigInteger('doctor_id')->nullable();
                $table->string('name');
                $table->string('referral_type')->nullable();
                $table->string('customer_phone_number')->nullable();
                $table->integer('number_of_tablet');
                $table->string('discount_type')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
