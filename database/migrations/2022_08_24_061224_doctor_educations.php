<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DoctorEducations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    if ( !Schema::hasTable('doctor_educations') ) {
        Schema::create('doctor_educations', function (Blueprint $table) {
            $table->id();
            $table->integer('doctor_id');
            $table->string('degree')->nullable();
            $table->string('institute_name')->nullable();
            $table->string('internship_institute')->nullable();
            $table->timestamps();
        });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('doctor_educations');
    }
}
