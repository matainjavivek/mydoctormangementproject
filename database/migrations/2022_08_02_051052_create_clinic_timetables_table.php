<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicTimetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('clinic_timetables') ) {
            Schema::create('clinic_timetables', function (Blueprint $table) {
                $table->id();
                $table->integer('clinic_id')->unsigned();
                $table->integer('doctor_id')->unsigned();
                $table->string('day');
                $table->string('start_time');
                $table->string('end_time');
                $table->dateTime('date')->nullable()->default(null);
                $table->timestamps();
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinic_timetables');
    }
}
