<?php
namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Utility;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        $arrPermissions = [
            'Manage Users',
            'Create User',
            'Edit User',
            'Delete User',

            'Manage Roles',
            'Create Role',
            'Edit Role',
            'Delete Role',

            'Manage Pharmacys',
            'Create Pharmacy',
            'Edit Pharmacy',
            'Delete Pharmacy',

            'Manage Doctors',
            'Create Doctor',
            'Edit Doctor',
            'Delete Doctor',

            'Manage Agents',
            'Create Agent',
            'Edit Agent',
            'Delete Agent',

            'Manage Orders',
            'Create Order',
            'Edit Order',
            'Delete Order',

            'Manage Reports',
            'Create Report',
            'Edit Report',
            'Delete Report',

            'Manage Medicines',
            'Create Medicine',
            'Edit Medicine',
            'Delete Medicine',

            'Manage Clinics',
            'Create Clinic',
            'Edit Clinic',
            'Delete Clinic',

            'Manage Permissions',
            'Create Permission',
            'Edit Permission',
            'Delete Permission',
            'Manage Languages',
            'Create Language',
            'Edit Language',
            'System Settings',
        ];

        foreach($arrPermissions as $ap)
        {
            Permission::create(['name' => $ap]);
        }

        $adminRole = Role::create(
            [
                'name' => 'Owner',
                'created_by' => 0,
            ]
        );

        $adminPermissions = [
            'Manage Users',
            'Create User',
            'Edit User',
            'Delete User',
            'Manage Roles',
            'Create Role',
            'Edit Role',
            'Delete Role',
            'Manage Pharmacys',
            'Create Pharmacy',
            'Edit Pharmacy',
            'Delete Pharmacy',
            'Manage Doctors',
            'Create Doctor',
            'Edit Doctor',
            'Delete Doctor',

            'Manage Agents',
            'Create Agent',
            'Edit Agent',
            'Delete Agent',

            'Manage Orders',
            'Create Order',
            'Edit Order',
            'Delete Order',

            'Manage Reports',
            'Create Report',
            'Edit Report',
            'Delete Report',

            'Manage Medicines',
            'Create Medicine',
            'Edit Medicine',
            'Delete Medicine',

            'Manage Clinics',
            'Create Clinic',
            'Edit Clinic',
            'Delete Clinic',

            'Manage Permissions',
            'Create Permission',
            'Edit Permission',
            'Delete Permission',
            'Manage Languages',
            'Create Language',
            'Edit Language',
            'System Settings',

        ];

        foreach($adminPermissions as $ap)
        {
            $permission = Permission::findByName($ap);
            $adminRole->givePermissionTo($permission);
        }
        $admin = User::create(
            [
                'name' => 'Admin',
                'email' => 'admin@example.com',
                'password' => Hash::make('1234'),
                'type' => 'Owner',
                'lang' => 'en',
                'created_by' => 0,
            ]
        );
        $admin->assignRole($adminRole);
        $admin->defaultEmail();
       // $admin->userDefaultData();

        // print_r($admin);die('+++++');

        $clientRole        = Role::create(
            [
                'name' => 'Client',
                'created_by' => 0,
            ]
        );
        $clientPermissions = [
            "Manage Pharmacys",

        ];
        foreach($clientPermissions as $ap)
        {
            $permission = Permission::findByName($ap);
            $clientRole->givePermissionTo($permission);
        }

        $userRole        = Role::create(
            [
                'name' => 'Employee',
                'created_by' => $admin->id,
            ]
        );
        $userPermissions = [
            'Manage Reports',

        ];

        foreach($userPermissions as $ap)
        {
            $permission = Permission::findByName($ap);
            $userRole->givePermissionTo($permission);
        }
        $user = User::create(
            [
                'name' => 'User',
                'email' => 'user@example.com',
                'password' => Hash::make('1234'),
                'type' => 'Employee',
                'lang' => 'en',
                'created_by' => $admin->id,
            ]
        );
        $user->assignRole($userRole);
        Utility::add_landing_page_data();
    }
}
